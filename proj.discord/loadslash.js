require('dotenv').config()
const Discord = require("discord.js");
const {ensureEnv} = require("./utils/functions");

const GUILD_ID = ensureEnv("GUILD_ID");
const DISCORD_BOT_TOKEN = ensureEnv("DISCORD_BOT_TOKEN");

const client = new Discord.Client({
    intents:[
        "GUILDS", 
        "GUILD_MESSAGES",
        "GUILD_MEMBERS"
    ]
});

const bot = {
    client
}

client.slashcommands = new Discord.Collection();
client.loadSlashcommands = (bot, reload)=>require("./command_bot/handlers/slashcommands")(bot, reload);
client.loadSlashcommands(bot, false);

client.on("ready", async ()=>{
    const guild = client.guilds.cache.get(GUILD_ID);
    if(!guild){
        return console.error(`guild ${GUILD_ID} not found`);
    }
    await guild.commands.set([...client.slashcommands.values()]);
    console.log(`Successfully loaded in ${client.slashcommands.size}`);
    process.exit(0);
})

client.login(DISCORD_BOT_TOKEN);

module.exports = bot;