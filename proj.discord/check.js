require('dotenv').config()
const Discord = require("discord.js");
const {getMysqlConnection} = require("./utils/db");
const {ensureEnv} = require("./utils/functions");

const GUILD_ID = ensureEnv("GUILD_ID");
const DISCORD_BOT_TOKEN = ensureEnv("DISCORD_BOT_TOKEN");

const client = new Discord.Client({
    intents:[
        "GUILDS", 
        "GUILD_MESSAGES",
        "GUILD_MEMBERS"
    ]
});

client.on("ready", async ()=>{
    
    // check guild id
    const guild = client.guilds.cache.get(GUILD_ID);
    if(!guild){
        return console.error(`guild ${GUILD_ID} not found`);
    }

    // check db
    let db = getMysqlConnection({
        HOST:ensureEnv("DB_HOST"), 
        USER:ensureEnv("DB_USER"), 
        PASS:ensureEnv("DB_PASS"), 
        NAME:ensureEnv("DB_NAME")
    });

    let row = await db.one("SELECT COUNT(*) as n FROM TB_DISCORD_USER");
    console.log("db result:"+JSON.stringify(row, null, 2));

    //check roles
    let roles = await db.one(`SELECT \`value\` FROM TB_CONFIG WHERE \`key\`='roles'`);
    if(roles.length==0){
        throw Error('roles config is empty');
    }
    roles = roles['value'];
    if(typeof roles == 'string'){
        roles = JSON.parse(roles);
    }
    for(const role_name in roles){
        const role = guild.roles.cache.get(roles[role_name]);
        if (!role) {
            throw Error(`Unable to fetch Discord's '${role_name}' role.`);
        }
    }

    process.exit(0);
});

client.login(DISCORD_BOT_TOKEN);