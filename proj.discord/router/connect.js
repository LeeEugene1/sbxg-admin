const express = require("express");
const { db } = require("../utils/db");
const { validationResult, param } = require("express-validator");

const router = express.Router();

router.get("/", async function (req, res) {
  res.render("invalid", {});
});

router.get(
  "/:random_hash?",
  param("random_hash").exists().notEmpty(),
  async function (req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw Error("invalid key");
      }

      const random_hash = req.params.random_hash;
      const { exist, logined } = await db.one(`SELECT EXISTS (SELECT user_id FROM TB_DISCORD_USER 
                                                            WHERE random_hash=?) AS exist,
                                                      EXISTS (SELECT user_id FROM TB_DISCORD_USER 
                                                            WHERE random_hash=?
                                                                  AND stage=3 
                                                                  AND wallet IS NOT NULL
                                                                  AND user_id IS NOT NULL) AS logined`, 
                                              [random_hash, random_hash]);
      if (!exist) throw Error("invalid key");
      if (!!logined) throw Error("invalid key"); 

      // 로그인 절차 처리
      await db.query(`UPDATE TB_DISCORD_USER 
                      SET stage=2, wallet=NULL, role_id=NULL, updated_at=NOW() 
                      WHERE random_hash=? AND stage IN (1,2)`,
        [random_hash]
      );

      return res.render("connect", { random_hash });
    } catch (error) {
      return res.render("invalid", { message: error.message });
    }
  }
);

module.exports = router;
