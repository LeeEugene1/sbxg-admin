const express = require("express");
const { validationResult, body, param } = require("express-validator");
const config = require("../../lib.common/config");
const { db } = require("../libs/db");
// const Roles = require("../utils/roles");
const infura = require("../../lib.common/infura");
let router = express.Router();

/**
 * index.js에서 discord의 client를 전달받습니다.
 * @param {Class} Roles 
 * @returns router
 */
const connectRouter = (roles) => {
  // const roles = new Roles(client, guild_id);

  router.post("/",
    body("random_hash").exists().notEmpty(),
    body("address").isEthereumAddress(),
    body("signature").exists().notEmpty(),
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty())
        return res.status(400).json({ message: "invalid params", errors: errors.array() });
  
      const message = config.server.SIGN_MESSAGE;
      const random_hash = req.body.random_hash;
      const address = req.body.address;
      const signature = req.body.signature;
  
      let web3 = infura.getWeb3(config.server.POLYGON_NETWORK);
      let hash = web3.utils.soliditySha3(message, address);
      const recovered = web3.eth.accounts.recover(hash, signature);
  
      if (address.toLowerCase() !== recovered.toLowerCase()) {
        return res.status(500).json({message: "internal server error"});
        
      } else {
        // TB_DISCORD_USER에 1:1로 연결되어 있는지 체크
        try {

          
          let {connected, user_id} = await db.one(`SELECT EXISTS (SELECT * FROM TB_DISCORD_USER WHERE wallet=? AND stage=3) AS connected,
                                                          (SELECT user_id FROM TB_DISCORD_USER WHERE random_hash=?) AS user_id`,
                                                          [recovered, random_hash]);

                                                          console.log(user_id, random_hash);
          if (!!connected) {
            throw new Error("already connected");
          }

          if(!user_id){
            throw new Error("empty user_id");
          }

          // Role
          await roles.initialize(db);
          const {grade} = await db.one(`SELECT IFNULL(MAX(grade),0) AS grade FROM TB_NFT WHERE holder=?`,[recovered]);
          const selected_grade = grade;
          let selected_role_id = await roles.getRoleByGrade(selected_grade);
          await roles.changeRoles(user_id, selected_role_id);

          //TB_DISCORD_USER 업데이트
          await db.query("UPDATE TB_DISCORD_USER SET wallet=?, stage=3, role_id=?, updated_at=NOW() WHERE random_hash=?",[recovered, selected_role_id, random_hash]);
  
        } catch (error) {
          console.log(error)
          return res.status(400).json({message: `${error.message}`});
        }
  
        return res.status(200).json({ success: true });
      }
    }
  );

  router.get("/add-holder-role/:user_id",
  param("user_id").exists().notEmpty(), 
  async(req, res)=>{
   
      try{
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
              throw Error("invalid key");
          }

          const user_id = req.params.user_id;

          // const {nft_count} = await db.one(`SELECT COUNT(token_id) AS nft_count FROM TB_NFT 
          //                                   WHERE holder=(SELECT wallet FROM TB_DISCORD_USER WHERE user_id=? AND stage=3)`,
          //                                   [user_id]);
          // if(parseInt(nft_count) === 0){ throw Error('You are not NFT Holder')}

          await roles.initialize(db);
          const {grade} = await db.one(`SELECT IFNULL(MAX(grade), 0) AS grade FROM TB_NFT WHERE holder=(SELECT wallet FROM TB_DISCORD_USER WHERE user_id=?)`,[user_id]);
          let selected_role_id = await roles.getRoleByGrade(grade);
          await roles.changeRoles(user_id, selected_role_id);

          return res.json({success: true});

      }catch(error){
        console.log(error.message);
        return res.status(400).json({success: false, error:error.message});
      }

  });


  router.get("/delete-all-roles/:user_id", 
  param("user_id").exists().notEmpty(), 
  async(req, res)=>{

    try{
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          throw Error("invalid key");
      }

      const user_id = req.params.user_id;
      await roles.initialize(db);
      await roles.changeRoles(user_id, null);

      return res.json({success: true});
    }catch(error){
      console.log(error.message);
      return res.status(400).json({success: false, error:error.message});
    }

  });



  return router;
}


module.exports=connectRouter;

