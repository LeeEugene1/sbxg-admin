/**
 * https://v13.discordjs.guide/popular-topics/faq.html#how-do-i-kick-a-user
*/
const cron = require('node-cron');
const {db} = require("../../utils/db");
const Roles = require("../../utils/roles");
const {ensureEnv} = require("../../utils/functions");
const GUILD_ID = ensureEnv("GUILD_ID");

const proc = async(roleCl)=>{

    try{

        // 모든 멤버의 정보를 가져와서 역할 Gold, Silver, Bronze삭제
        // 연동된 멤버(nft 보유자)만 역할 부여
        await roleCl.initialize(db);
        await roleCl.deleteAllMemeberRoles();

        const users = await db.query(`SELECT MAX(n.grade) AS grade, n.holder, u.user_id, u.name, u.role_id
                                        FROM TB_NFT AS n 
                                        INNER JOIN 
                                             TB_DISCORD_USER AS u 
                                        ON n.holder = u.wallet 
                                        WHERE u.stage=3 
                                        GROUP BY n.holder`);
        console.log(`update users num: `+users.length)
        for (const user of users) {

            const user_id = user['user_id'];
            const user_grade = user['grade'];
            try{

                let selected_role_id = await roleCl.getRoleByGrade(user_grade);
                await roleCl.addRole(user_id, selected_role_id);

                await db.query(`UPDATE TB_DISCORD_USER SET role_id=? WHERE user_id=?`, [selected_role_id, user_id])

            }catch(err){
                console.log(`add role error: user_id: ${user_id}, message: ${err.message}`)
            }
        }

    }catch(error){
        console.log(`proc error: `+error.message);
    }
}

async function sync_roles (client){
    cron.schedule('0 0 * * *', async () => { // UTC 0시, KST 오전 9시
        console.log(`>> start synct roles [${new Date()}]`);
        await proc(new Roles(client, GUILD_ID)); 
        console.log('>> finished synct roles');
    });
}

module.exports={sync_roles};