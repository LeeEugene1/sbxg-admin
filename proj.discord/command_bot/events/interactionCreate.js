module.exports = {
    name:"interactionCreate",
    run:async (bot, interaction)=>{
        const {client} = bot;
        if(!interaction.isCommand())return;
        if(!interaction.inGuild()){            
            return interaction.reply("this command can only in guild");
        }
        
        const slashcmd = client.slashcommands.get(interaction.commandName);
        if(!slashcmd) return interaction.reply("Invalid slash command")
        
        if(slashcmd.permission && !interaction.memeber.permission.has(slashcmd.permission)){
            return interaction.reply("you do not have permmision");
        }

        slashcmd.run(client, interaction);
    }
}