const {sync_roles} = require("../cron/sync_roles");
module.exports = {
    name:"ready",
    run: async (bot) => {
        console.log("logged in as "+bot.client.user.tag);
        sync_roles(bot.client);
    }
}