const Discord = require("discord.js");

/**
 * @param {*} bot : context object
 * @param {Discord.Message} message 
 * @returns 
 */

async function run(bot, message) {
    const {client, prefix, owners} = bot;

    if(!message.guild) 
        return;
    if(message.author.bot) 
        return;
    if(!message.content.startsWith(prefix)) 
        return;

    
    const args = message.content.slice(prefix.length).trim().split(/[\s]+/g)
    const cmdstr = args.shift().toLowerCase();        
    //ex) n.ping hello
    const command =  client.commands.get(cmdstr);
    if(!command)
        return
    let member = message.member;
    
    if(command.devOnly && !owners.includes(member.id)){
        return message.reply("this command only available to bot owners");
    }

    // Permissions ref : https://discord.com/developers/docs/topics/permissions#permissions-bitwise-permission-flags
    const missed = member.permissions.missing(command.permissions);
    if(command.permissions && missed.length !== 0){
        return message.reply("you do not have permission to use this command");
    }

    try{
        await command.run({...bot, message, args})
    }catch(err){
        let errMsg =err.toString();
        if(errMsg.startWith("?")) {
            errMsg = errMsg.slice(1);
            message.reply(errMsg);
        }else
            console.err(err);
    }
    
} 

module.exports = {
    name:"messageCreate",
    run
}