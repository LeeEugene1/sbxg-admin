const Discord = require("discord.js");
const validator = require('validator').default;
const {db} = require("../../utils/db");
const {getEndPoint} = require("../../utils/functions");
const endpoint = getEndPoint();

const config = {
    name:"wallet",
    description:"connect wallet address",
    permissions:[],
    options: [
        {
            name:"connect", 
            description:"Connect your Wallet to your Discord account. Discord 계정과 지갑을 연결합니다.",
            type:1
        },
        {
            name: "disconnect",
            description:"Disconnect your Wallet from your Discord account. Discord 계정과 지갑의 연결을 해제합니다.",
            type:1
        },
        {
            name: "status",
            description:"Check which Wallet is connected to your Discord Account. 현재 Discord 계정과 연동된 지갑의 정보를 확인합니다.",
            type:1
        },
        {
            name:"verify-holder", 
            description:`Add the role of 'Holder' depending on whether you are an NFT holder. NFT 보유 유무에 따라서 '홀더' 역할을 부여받습니다.`,
            type:1, 
        },
    ]
}

/**
 * 
 * @param {Discord.Client} client 
 * @param {Discord.Interaction} interaction 
 */
async function run (client, interaction) {
    const userTag = interaction.member.user.tag;
    const userId = interaction.member.user.id;

    let subcmd = interaction.options.getSubcommand("connect");
    let row = await db.one("SELECT * FROM TB_DISCORD_USER WHERE user_id=? AND stage=3 limit 1", [userId]);

    switch(subcmd)
    {
        case "connect":

            if(!row){

                let random_hash = null;
                while(1){
                    random_hash = Math.random().toString(36).substring(2, 12); // 길이 12 이상 생성 불가
                    let {exist_hash} = await db.one(`SELECT EXISTS (SELECT random_hash FROM TB_DISCORD_USER WHERE random_hash=?) as exist_hash`,[random_hash]);
                    if(!exist_hash) break;
                }

                let q = `INSERT INTO TB_DISCORD_USER 
                            SET random_hash=?, user_id=?, name=?, created_at=NOW() 
                            ON DUPLICATE KEY UPDATE random_hash=?, updated_at=NOW()`;
                await db.query(q, [random_hash, userId, userTag, random_hash]);

                return interaction.reply({
                    "components": [
                        {
                            "type": 1,
                            "components": [
                                {
                                    "type": 2,
                                    "label": "Connect Wallet (지갑 연결)",
                                    "style": 5,
                                    "url": `${endpoint}/connect/${random_hash}`
                                },
                                {
                                    "type": 2,
                                    "label": "Guide (가이드)",
                                    "style": 5,
                                    "url": `${endpoint}/guide`
                                }
                            ]
                        }
                    ],
                    ephemeral: true // 본인만 볼 수 있음
                });

               
            }else{
                return interaction.reply({
                            content:"This wallet has already been connected, please disconnect your wallet from the Discord ID to continue.\n이미 지갑이 연결되어 있습니다. 다른 지갑을 연결하시려면 현재 연결된 지갑을 먼저 연결 해제해 주세요.", 
                            ephemeral:true
                        });
            }

        case "disconnect":
            if(row){
                let text = "Your wallet has been disconnected.\n지갑과의 연결이 해제되었습니다.";
                const response = await fetch(`${endpoint}/auth/delete-all-roles/${userId}`);
                let result = await response.json();
                if(!!result['success']){
                    await db.query("DELETE FROM TB_DISCORD_USER WHERE user_id=?", [userId]);
                }else{
                    text = `Failed to disconnect wallet. try again.\n지갑 연결에 실패했습니다. 다시 시도해주세요.\nError: ${result['error']}`
                }
                return interaction.reply({
                    content: text,
                    ephemeral: true
                });

            }else{
                return interaction.reply({content:"There is no wallet connected to your account.\n계정에 연결된 지갑이 없습니다.", ephemeral:true})
            }

        case "status":
            if(!row){
                return interaction.reply({
                    content:"You need to connect your wallet first.\n지갑 연결을 먼저 진행하셔야 합니다.", 
                    ephemeral: true
                });
            }else{
                let text = `Your wallet address is\n연결된 지갑 주소는 다음과 같습니다.\n\`${row.wallet}\``;
                return interaction.reply({
                    content:text,
                    ephemeral: true
                });
            }
        case "verify-holder":
            if(!row){
                return interaction.reply({
                    content:"Authentication failed. You need to connect your wallet first.\n인증 실패. 지갑 연결을 먼저 진행하셔야 합니다.", 
                    ephemeral: true
                });
            }else{
            
                let text = `You are not NFT Holder.\nNFT 홀더가 아닙니다.`;
                const {nft_count} = await db.one(`SELECT COUNT(token_id) AS nft_count FROM TB_NFT 
                                                  WHERE holder=(SELECT wallet FROM TB_DISCORD_USER WHERE user_id=? AND stage=3)`,[userId]);
                if(parseInt(nft_count)>0){
                    let response = await fetch(`${endpoint}/auth/add-holder-role/${userId}`);
                    let result = await response.json();
                    text = result['success']? 
                    `The 'Holder' role has been added.\nThe quantity of owned NFTs is ${nft_count}.\n'홀더' 역할이 부여되었습니다.\n지갑에 보유중인 NFT 수량은 ${nft_count}개 입니다.` : 
                    `Failed to add Holder Role.\nThe quantity of owned NFTs is ${nft_count}.\n'홀더' 역할 부여에 실패했습니다.\n지갑에 보유중인 NFT 수량은 ${nft_count}개 입니다.\nError: ${result['error']}`;
                }

                return interaction.reply({
                    content:text,
                    ephemeral: true
                });
            }

    }
}

module.exports = {...config, run }