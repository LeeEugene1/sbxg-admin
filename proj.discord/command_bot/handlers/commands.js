
const {getFiles} = require("../../utils/functions");
const fs = require("fs");

module.exports = (bot, reload)=>{
    console.log("start load commands from ./commands folder");
    const {client} = bot;
    fs.readdirSync("./command_bot/commands/").forEach((category)=>{
        let commands = getFiles(`./command_bot/commands/${category}`, ".js");
        commands.forEach((f,i)=>{
            if(reload)
                delete require.cache[require.resolve(`../commands/${category}/${f}`)]
            
            const command = require(`../commands/${category}/${f}`);
            client.commands.set(command.name, command);

            console.log(`${i+1}. ${f} loaded`);
        })
    })
    console.log(`loaded ${client.commands.size} commands`);
}