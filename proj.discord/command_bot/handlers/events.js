const  {getFiles} = require("../../utils/functions");

module.exports = (bot, reload) => {
    console.log("start load events from ./events folder");
    const {client} = bot;
    let events = getFiles("./command_bot/events/", ".js");

    if(events.length === 0){
        console.log("No events to load");
    }

    events.forEach((f,i)=>{
        
        if(reload)
            delete require.cache[require.resolve(`../events/${f}`)]

        const event = require(`../events/${f}`)
        client.events.set(event.name, event);

        if(!reload)
            console.log(`${i+1}. ${f} loaded`);
    });

    console.log(`loaded ${client.events.size} events`);    

    if(!reload)
        initEvents(bot)
    
}

function triggerEventHandler(bot, event, ...args) {
    const {client} = bot;
    try {
        if(client.events.has(event))
            client.events.get(event).run(bot, ...args)
        else
            throw new Error(`event ${event} does not exist`);
    }catch(err){
        console.error(err);
    }
}

function initEvents(bot) {
    const {client} = bot;

    client.on("ready", ()=>{
        triggerEventHandler(bot, "ready");
    });

    // client.on("messageCreate", (message)=>{
    //     triggerEventHandler(bot, "messageCreate", message);
    // });

    // 메시지를 받으면 호출되는 함수
    client.on("interactionCreate", (interaction)=>{
        triggerEventHandler(bot, "interactionCreate", interaction);
    });
}