const {getFiles} = require("../../utils/functions")

module.exports = (bot, reload) => {
    console.log("start load slash-commands from ./slashcommands folder");
    const {client} = bot;
    let slashcommands = getFiles("./command_bot/slashcommands/", ".js");

    if(slashcommands.length === 0){
        console.log("No slash-commands to load");
    }

    slashcommands.forEach((f,i)=>{
        
        if(reload)
            delete require.cache[require.resolve(`../slashcommands/${f}`)]

        const slashcommand = require(`../slashcommands/${f}`)
        client.slashcommands.set(slashcommand.name, slashcommand);

        if(!reload)
            console.log(`${i+1}. ${f} loaded`);
    });
}