# 개요
sheepfarm discord bot

# 프로젝트 구조

* [discord-bot]
``` 
proj.discord
├───[command_bot]───[handlers]
|               ├───[commands]
|               ├───[events]
|               ├───[slashcommands]
|               ├───[cron]
├───[utils]
├───.env
├───loadslash.js
├───check.js
└───index.js
```
handler 에서 commands, events, slashcommands 폴더에 있는 파일들로 연결해주는 구조입니다.

* events : 새로운 이벤트를 처리하고 싶다면 이곳에 파일 추가하고 `./command_bot/handlers/events.js` 하단에 이벤트를 추가합니다.
* commands : 새로운 커맨드를 등록하고 싶다면 이곳에 파일을 추가합니다. 이벤트 처리 과정에서 불러와 사용 합니다. `messageCreate.js` 파일 참고
* slashcommands : 새로운 슬래시 커맨드를 등록하고 싶다면 이곳에 파일을 추가합니다. 슬래시 커맨드는 디스코드에서 지원하는 `/`로 시작하는 명령어 시스템입니다. description 이나 옵션을 포함하여 디스코드에 설정 할 수 있습니다.
* cron: 스케줄러 처리 코드입니다.
* utils: 공용으로 사용되는 코드입니다.

* [web]
```
proj.discord
├───views
├───router
├───index.js
```
월렛 인증을 위한 웹사이트입니다.

# 환경설정

`.env` 파일을 만들고 다음 내용을 채웁니다.

```
DISCORD_BOT_TOKEN=...
GUILD_ID=... #server_id
DB_HOST=...
DB_USER=...
DB_PASS=...
DB_NAME=...
PORT=5001
CONNECT_ENDPOINT=http://...
POLYGON_NETWORK=mumbai
JWT_SECRET=
```

로컬에서 사용 할 DISCORD_BOT_TOKEN 은 [Discord developer portal](https://discord.com/developers/applications/) 에 방문해서 Application과 Bot 을 만들어서 얻을 수 있음.

# 실행

환경파일 `.env` 검사 

```
node check.js
```

디스코드 봇 실행

```
node index.js
```

디스코드 슬래시 커맨드 재등록

```
node loadslash.js
```


# reference

* [discord bot documentation](https://discord.com/developers/docs/resources)

* [Slash command Options](https://discord.com/developers/docs/interactions/application-commands#application-command-object-application-command-option-structure)