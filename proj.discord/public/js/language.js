const language = {
  eng: {
    error: `<h1>Sorry!</h1>
        <p>This is an invalid approach. Please try again from the beginning.</p>
        `,
    welcome: `
        <h1 id="title">Welcome!</h1>
            <p id="description">Click the button to link your Discord account with your MetaMask wallet.</p>
    `,
    guide: `
        <span>Need help?</span>
        <a href="https://bit.ly/3JQIpHK" target="_blank"><span>Go to the guidebook</span></a>
    `,
    success: `
        <h1>Thank You!</h1>
        <p>Your Discord account and wallet are now linked.</p>
    `,
    already: `
        <h1>Sorry</h1>
        <p>A wallet that is already linked.</p>
    `,
    fail: `<h1>Sorry!</h1>
        <p>An error occurred.</p>
        `,
  },
  ko: {
    error: `
        <h1>죄송합니다.</h1>
        <p>올바르지 않은 접근입니다. 처음부터 다시 시도해주세요.</p>
        `,
    welcome: `
        <h1 id="title">환영합니다!</h1>
            <p id="description">버튼을 눌러 디스코드계정과 메타마스크 지갑을 연동하세요.</p>
    `,
    guide: `
    <span>도움이 필요하신가요?</span>
    <a href="https://bit.ly/3PWHsBy" target="_blank"><span>가이드북 바로가기</span></a>
    `,
    success: `
        <h1>감사합니다.</h1>
        <p>디스코드 계정과 지갑 연동이 완료되었습니다.</p>
    `,
    already: `
        <h1>죄송합니다.</h1>
        <p>이미 연동된 지갑입니다.</p>
    `,
    fail: `
        <h1>죄송합니다.</h1>
        <p>에러가 발생하였습니다.</p>
    `,
  },
};

function changeLanguage(lang) {
  window.sessionStorage.setItem("language", lang);
  const eng = document.getElementById("eng");
  const ko = document.getElementById("ko");

  if (lang === "eng") {
    eng.classList.add("active");
    ko.classList.remove("active");
  } else {
    ko.classList.add("active");
    eng.classList.remove("active");
  }
}

function drawContent(type) {
  const isLanguage = window.sessionStorage.getItem("language");
  const lang = isLanguage === "" ? "eng" : isLanguage;
  return language[lang][type];
}
