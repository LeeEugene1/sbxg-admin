const eng = document.getElementById("eng");
const ko = document.getElementById("ko");
const title = document.getElementById("title");
const guide = document.getElementById("guide");
const btn = document.querySelector("#handleConnect");

const isMetamaskInstalled = () => {
  if (!window.ethereum) {
    let webStoreUrl =
      "https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en";

    window.open(webStoreUrl, "_blank");
    return false;
  } else {
    return true;
  }
};

async function handleConnect() {
  //const 화살표함수의 경우 재귀호출불가
  const connectNetwork = async (chainId, chainName, rpcUrls) => {
    try {
      await window.ethereum.request({
        method: "wallet_switchEthereumChain",
        params: [
          {
            chainId,
          },
        ],
      });
    } catch (switchError) {
      if (switchError.code === 4902) {
        try {
          await window.ethereum.request({
            method: "wallet_addEthereumChain",
            params: [
              {
                chainId,
                chainName,
                rpcUrls: [rpcUrls],
              },
            ],
          });
          return true; //'Network added and switched successfully';
        } catch (addError) {
          throw addError;
        }
        return handleConnect();
      } else {
        throw new Error("canceled");
      }
    } finally {
      let isLocked = await window.ethereum._metamask.isUnlocked();
      if (!isLocked) {
        //잠금해제후 다시시작
        await window.ethereum.request({
          method: "eth_requestAccounts",
        });
        return handleConnect();
      }
      let walletAddress = await window.ethereum.request({
        method: "eth_requestAccounts",
      });

      const lan = window.sessionStorage.getItem("language");
      if (lan === "ko") {
        if (!confirm(`연동하려는 지갑이 맞습니까? ${walletAddress[0]}`)) {
          return false;
        }
      } else {
        if (
          !confirm(`Is this the wallet you want to link? ${walletAddress[0]}`)
        ) {
          return false;
        }
      }
      const web3 = new Web3(window.ethereum);
      const hash2 = web3.utils.soliditySha3("sbxg-sign", walletAddress[0]);

      const signature = await window.ethereum
        .request({
          method: "personal_sign",
          params: [hash2, walletAddress[0]],
        })
        .catch(console.log);

      if (!signature) return false;

      await fetch(`/auth`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          address: walletAddress[0],
          signature: signature,
          random_hash: '<%= random_hash %>}',
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.message) {
            throw new Error(data.message);
            return false;
          }
          if (data.success) {
            handleSuccess();
            btn.style.display = "none";
          }
        })
        .catch((err) => {
          if (err.message.includes("already")) {
            handleAlready();
          } else {
            handleError(err.message);
          }
          console.log(err.message);
        });
    }
  };

  const handleError = (msg) => {
    let obj = { title: "fail", guide: "guide" };
    window.sessionStorage.setItem("type", JSON.stringify(obj));

    title.innerHTML = drawContent("fail");
    guide.innerHTML = drawContent("guide");
  };
  const handleAlready = () => {
    let obj = { title: "already", guide: "guide" };
    window.sessionStorage.setItem("type", JSON.stringify(obj));

    title.innerHTML = drawContent("already");
    guide.innerHTML = drawContent("guide");
  };
  const handleSuccess = () => {
    let obj = { title: "success", guide: "guide" };
    window.sessionStorage.setItem("type", JSON.stringify(obj));

    title.innerHTML = drawContent("success");
    guide.innerHTML = drawContent("guide");
  };

  if (!isMetamaskInstalled()) {
    return false;
  }

  if (
    window.location.href.includes("localhost") ||
    window.location.href.includes("test")
  ) {
    connectNetwork(
      "0x13881", //80001
      "Polygon Mumbai",
      "https://mumbai.polygonscan.com/"
    );
  } else {
    connectNetwork(
      "0x89", //137
      "Polygon Mainnet",
      "https://polygon-rpc.com/"
    );
  }
}

btn.addEventListener("click", () => {
  handleConnect();
});
