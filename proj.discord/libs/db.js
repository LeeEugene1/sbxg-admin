const path = require("path");
const mysql = require("../../lib.common/mysql");
const checkEnv = require("check-env");
require("dotenv").config({ path: path.join(__dirname, ".env") });

// checkEnv([DB_HOST, DB_USER, DB_PASS, DB_NAME]);

const db = new mysql.MySQL(
  process.env.DB_HOST,
  process.env.DB_USER,
  process.env.DB_PASS,
  process.env.DB_NAME
);

console.log("db_name", process.env.DB_NAME);

module.exports = { db, ...mysql };
