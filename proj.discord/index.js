const express = require("express");
const cors = require("cors");
const app = express();
const Roles = require("./utils/roles");
app.set("view engine", "ejs");

require("dotenv").config();
const Discord = require("discord.js");
const { ensureEnv } = require("./utils/functions");
const DISCORD_BOT_TOKEN = ensureEnv("DISCORD_BOT_TOKEN");
const GUILD_ID = ensureEnv("GUILD_ID");

const client = new Discord.Client({
  intents: ["GUILDS", "GUILD_MESSAGES", "GUILD_MEMBERS"],
});

let bot = {
  client,
  prefix: "n.",
  owners: ["1128222868774322267"],
};

client.commands = new Discord.Collection();
client.events = new Discord.Collection();
client.slashcommands = new Discord.Collection();

client.loadEvents = (bot, reload) =>
  require("./command_bot/handlers/events")(bot, reload);
client.loadEvents(bot, false);

client.loadCommands = (bot, reload) =>
  require("./command_bot/handlers/commands")(bot, reload);
client.loadCommands(bot, false);

client.loadSlashcommands = (bot, reload) =>
  require("./command_bot/handlers/slashcommands")(bot, reload);
client.loadSlashcommands(bot, false);

client.login(DISCORD_BOT_TOKEN);

app.use(cors());
// app.use(cookieParser());
app.use(express.static("public"));
app.use(express.json({ limit: "50mb" }));
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.urlencoded({ extended: true }));

const router = express.Router();

// router.get("/", async function (req, res) {
//   res.render("index", {});
// });

app.use("/", router);
app.get("/guide", (req, res) => {
  try {
    return res.render(__dirname + `/views/guide.ejs`);
  } catch (error) {
    console.log(error);
  }
});
app.use("/auth/", require("./router/auth.js")(new Roles(client, GUILD_ID)));
app.use("/connect/", require("./router/connect.js"));

app.get("*", (req, res) => {
  return res.render("invalid", { message: "404" });
});

let port = process.env.PORT;
app.listen(port, () => {
  console.log(`The Express server is listening at PORT: ${port}`);
});

module.exports = bot;
