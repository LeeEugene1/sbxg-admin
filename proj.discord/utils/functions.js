require("dotenv").config();
const fs = require("fs");
const getFiles = (path, ending)=>{
    return fs.readdirSync(path).filter(f=>f.endsWith(ending))
}

const ensureEnv = (name) => {
    if(name in process.env == false)
        throw new Error(`process.env.${name} should be specified`);
    return process.env[name];
}

const getEndPoint = ()=>{
    return process.env.CONNECT_ENDPOINT
}

module.exports = {getFiles, ensureEnv, getEndPoint}