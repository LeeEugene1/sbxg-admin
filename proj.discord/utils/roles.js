class Roles{

    constructor(_client, _guild_id){
        if(!_client){
            throw Error(`client is null`);
        }
        this.client = _client;
        this.guild_id = _guild_id;
    }

    /**
     * 클래스 생성 후, 다른 함후 호출 전에 먼저 실행해줍니다.
     */
    async initialize(db) {
        
        if(!db){
            db = require("./db").db;
        }

        let roles = await db.one(`SELECT \`value\` FROM TB_CONFIG WHERE \`key\`='roles'`);
        if(roles.length==0){
            throw Error('roles config is empty');
        }
        roles = roles['value'];
        if(typeof roles == 'string'){
            roles = JSON.parse(roles);
        }

        this.gold_role_id = roles.gold; 
        this.silver_role_id = roles.silver; 
        this.bronze_role_id = roles.bronze; 
        this.holder_role_id = roles.holder; 
        this.role_ids = [this.gold_role_id, this.silver_role_id, this.bronze_role_id, this.holder_role_id];
        this.guild=this.client.guilds.cache.get(this.guild_id);
        this.initialized=true;
    }

    async getRoleByGrade(grade){
        // console.log('getRoleByGrade')
        if(!grade) return null;

        let selected_role_id = this.bronze_role_id;
        if(20 == grade){ selected_role_id = this.silver_role_id; }
        if(30 == grade){ selected_role_id = this.gold_role_id; }
        return selected_role_id;
    }

    async #getClientMember(member_id){
        let member = this.guild.members.cache.get(member_id);
        if (!member) {
            member = await this.guild.members.fetch(member_id); 
        }
        return member;
    }

    //////// sync
    /**
     * 서버에 있는 모든 멤버들의 역할(Role)을 삭제합니다.
     * Role: Gold, Silver, Bronze, Holder
     */
    async deleteAllMemeberRoles(){
        if(!this.initialized){
            throw Error('Please execute initialize function')
        }
        const members = await this.guild.members.fetch();
        if(!!members){
            const filteredMembers = members.filter(member => !member.user.bot);
            filteredMembers.forEach((member) => {
                // console.log(member.user.username, member.user.id);
                if (member._roles.length>0) {
                    member._roles.forEach(async(role_id) => {
                        if(this.role_ids.includes(role_id)){
                            await member.roles.remove(role_id);
                        }
                    });
                }
            });
        }
        
    }

    /**
     * 역할을 추가합니다.
     * @param {string} member_id 
     * @param {string} selected_role_id 
     * @returns 
     */
    async addRole(member_id, selected_role_id){
        if(!this.initialized){
            throw Error('Please execute initialize function')
        }
        if(!selected_role_id) return;
        const member = await this.#getClientMember(member_id);
        if (!!member) {
            await member.roles.add(selected_role_id);
            await member.roles.add(this.holder_role_id);
        }
    }

    /**
     * 홀더 역할을 추가합니다.
     * @param {string} member_id 
     */
    async addHolderRole(member_id){
        if(!this.initialized){
            throw Error('Please execute initialize function')
        }
        // console.log('addHolderRole')
        const member = await this.#getClientMember(member_id);
        if (!!member) {
            await member.roles.add(this.holder_role_id);
        }
    }

    /////// auth
    async changeRoles(member_id, selected_role_id){
        if(!this.initialized){
            throw Error('Please execute initialize function')
        }
        const member = await this.#getClientMember(member_id);
        if (member._roles.length>0) {
            member._roles.forEach(async(role_id) => {
                if(this.role_ids.includes(role_id)){
                    await member.roles.remove(role_id);
                }
            });
        }

        if(!selected_role_id) return;
        
        await member.roles.add(selected_role_id);
        await member.roles.add(this.holder_role_id);
        
    }
    

}

module.exports=Roles;