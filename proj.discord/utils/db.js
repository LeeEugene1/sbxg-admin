const mysql = require("../../lib.common/mysql");

function getMysqlConnection(option) { 
    // console.log("DB CONNECT TO:" + JSON.stringify(option, null, 2));
    return new mysql.MySQL(option["HOST"], option["USER"], option["PASS"], option["NAME"]);
}

const db = getMysqlConnection({
    HOST:process.env.DB_HOST, 
    USER:process.env.DB_USER, 
    PASS:process.env.DB_PASS, 
    NAME:process.env.DB_NAME
});

module.exports = {...mysql, db, getMysqlConnection};
