import { Modal } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
const { confirm } = Modal

export async function getWithAuth(url) {
    const { jwt_token } = JSON.parse(sessionStorage.getItem('auth'))
    const option = {
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            Pragma: 'no-cache',
            Authorization: jwt_token,
        },
    }
    const res = await fetch(url, option)
    const data = await res.json()
    if (res.status === 200) {
        return data
    } else {
        throw Error(data.message)
    }
}

export async function postWithAuth(url, bodyObj, method = 'post') {
    const { jwt_token } = JSON.parse(sessionStorage.getItem('auth'))
    const option = {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            Authorization: jwt_token,
        },
        body: JSON.stringify(bodyObj),
    }
    const res = await fetch(url, option)
    const data = await res.json()
    if (res.status === 200) {
        return data
    } else {
        throw Error(data.message)
    }
}

export const showConfirm = async (content) => {
    return new Promise((res, rej) => {
        confirm({
            icon: <ExclamationCircleOutlined />,
            content,
            onOk() {
                console.log('OK')
                res()
            },
            onCancel() {
                rej(new Error('Cancelled'))
            },
        })
    })
}

export const warning = (content) => {
    Modal.warning({
        title: 'Warning',
        content,
    })
}

export const error = (content) => {
    Modal.error({
        title: 'Error',
        content,
    })
}

export const randomNumber = async () => {
    return Math.floor(Math.random() * 20) + 1
}
