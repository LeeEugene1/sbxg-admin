import {
    PartitionOutlined,
    TeamOutlined,
    HomeOutlined,
    UserOutlined,
    SettingOutlined,
    DownloadOutlined,
} from '@ant-design/icons'
import { Breadcrumb, Layout, Menu, theme, Button, Row, Col, Avatar, Space, Typography } from 'antd'
import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { useEffect, useState } from 'react'
import detectEthereumProvider from '@metamask/detect-provider'
import Privileges from './pages/Privileges'
import Nft from './pages/Nft'
import Web3 from 'web3'
import NoRight from './components/NoRight'
import { error } from './js/common'
import Home from './pages/Home'
import Setting from './pages/Setting'

const { Header, Content, Footer, Sider } = Layout

const items2 = [
    // { key: `home`, icon: React.createElement(HomeOutlined), label: `Home` },
    { key: `nft`, icon: React.createElement(PartitionOutlined), label: `NFT` },
    { key: `previlege`, icon: React.createElement(TeamOutlined), label: `Previlege` },
    { key: `setting`, icon: React.createElement(SettingOutlined), label: `Setting` },
    { key: `guide`, icon: React.createElement(DownloadOutlined), label: `Guide` },
]

const App = () => {
    const {
        token: { colorBgContainer },
    } = theme.useToken()

    const [hasProvider, setHasProvider] = useState(null)
    const [isAdmin, setAdmin] = useState(false)
    const initialState = { accounts: [], chainId: '' }
    const [wallet, setWallet] = useState(initialState)

    useEffect(() => {
        const refreshAccounts = () => {
            console.log('refreshAccounts()')
            handleDisConnect()
        }
        const refreshChain = () => {
            console.log('refreshChain()')
            handleDisConnect()
        }
        const isAdminCheck = async () => {
            if (sessionStorage.getItem('auth')) {
                // && window.ethereum.isConnected()
                //auth있으면 로그인 유지
                setAdmin(true)
                const addr = await window.ethereum.request({
                    method: 'eth_accounts',
                })
                setWallet({ accounts: addr, chainId: 'polygon' })
            }
        }
        const getProvider = async () => {
            if (!isMetamaskInstalled()) {
                return false
            }
            const provider = await detectEthereumProvider({
                silent: true,
            })
            setHasProvider(provider._state.isConnected) //boolean

            if (provider) {
                isAdminCheck() //새로고침마다
                window.ethereum.on('accountsChanged', refreshAccounts)
                window.ethereum.on('chainChanged', refreshChain)
            }
        }

        getProvider()
        return () => {
            window.ethereum?.removeListener('accountsChanged', refreshAccounts)
            window.ethereum?.removeListener('chainChanged', refreshChain)
        }
    }, []) //언마운트될때 딱 1번 실행

    const isMetamaskInstalled = () => {
        if (!window.ethereum) {
            let webStoreUrl =
                'https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en'

            window.open(webStoreUrl, '_blank')
            return false
        } else {
            return true
        }
    }

    const handleDisConnect = () => {
        //not admin
        setAdmin(false)
        setWallet(initialState)
        sessionStorage.removeItem('auth')
    }

    const handleConnect = async (networkVersion = 'baobab') => {
        const connectNetwork = async (chainId, chainName, rpcUrls) => {
            try {
                await window.ethereum.request({
                    method: 'wallet_switchEthereumChain',
                    params: [
                        {
                            chainId,
                        },
                    ],
                })
            } catch (switchError) {
                if (switchError.code === 4902) {
                    try {
                        await window.ethereum.request({
                            method: 'wallet_addEthereumChain',
                            params: [
                                {
                                    chainId,
                                    chainName,
                                    rpcUrls: [rpcUrls],
                                },
                            ],
                        })
                        return true //'Network added and switched successfully';
                    } catch (addError) {
                        throw addError
                    }
                } else {
                    throw new Error('canceled')
                }
            } finally {
                let auth_token = `${process.env.REACT_APP_AUTH}`
                let walletAddress = await window.ethereum.request({
                    method: 'eth_requestAccounts',
                })

                const web3 = new Web3(window.ethereum)
                const hash2 = web3.utils.soliditySha3(auth_token, walletAddress[0])

                const signature = await window.ethereum
                    .request({
                        method: 'personal_sign',
                        params: [hash2, walletAddress[0]],
                    })
                    .catch(console.log)

                await fetch(`${process.env.REACT_APP_URL}auth`, {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        address: walletAddress[0],
                        signature: signature,
                    }),
                })
                    .then((res) => res.json())
                    .then((data) => {
                        if (data.error || data.message.includes('invalid')) {
                            //not admin
                            error('로그인 실패. 어드민 계정으로 다시 시도해주세요.')
                            handleDisConnect()
                            return false
                        }
                        console.log(data)
                        sessionStorage.setItem('auth', JSON.stringify(data))
                        setAdmin(true)
                        setWallet({ accounts: walletAddress, chainId })
                    })
                    .catch((err) => {
                        console.log(err)
                    })
            }
        }

        if (!isMetamaskInstalled()) {
            return false
        }

        if (process.env.REACT_APP_URL.includes('localhost') || process.env.REACT_APP_URL.includes('test')) {
            connectNetwork(
                '0x13881', //80001
                'Polygon Mumbai',
                'https://rpc-mumbai.maticvigil.com'
            )
        } else {
            connectNetwork(
                '0x89', //137
                'Polygon Mainnet',
                'https://polygon-rpc.com/'
            )
        }
    }

    const [itemChange, setItemChange] = useState('nft')

    return (
        <Layout>
            <Header className="nav">
                <div className="logo">
                    <a href="/">SBXG Admin</a>
                </div>
                {wallet.accounts.length < 1 /* Updated */ && (
                    <Button className="connect" onClick={handleConnect}>
                        <img src="/img/metamask.svg" alt="metamask" className="h-6 w-6" height={20} width={20} />
                        Connect MetaMask
                    </Button>
                )}

                {wallet.accounts.length > 0 && (
                    <div className="profile">
                        <Avatar
                            src={`https://xsgames.co/randomusers/avatar.php?g=pixel&key=${wallet.accounts[0].slice(
                                -1
                            )}`}
                            size="small"
                        />
                        {wallet.accounts[0].substring(0, 5)}...{wallet.accounts[0].slice(-4)}
                        <Button onClick={handleDisConnect}>Disconnect</Button>
                    </div>
                )}
            </Header>
            {isAdmin ? (
                <Content
                    style={{
                        padding: '0 50px',
                    }}
                >
                    <Breadcrumb
                        style={{
                            margin: '16px 0',
                        }}
                    >
                        <Breadcrumb.Item>Menu</Breadcrumb.Item>
                        <Breadcrumb.Item>{items2.filter((item) => item.key === itemChange)[0].label}</Breadcrumb.Item>
                    </Breadcrumb>
                    <Layout
                        style={{
                            padding: '24px',
                            background: colorBgContainer,
                            minHeight: 'calc(100vh - 100px)',
                        }}
                    >
                        <Sider
                            breakpoint={'xs'}
                            style={{
                                background: colorBgContainer,
                            }}
                            width={200}
                        >
                            <Menu
                                mode="inline"
                                defaultSelectedKeys={[itemChange]}
                                onClick={(item, key, keyPath, domEvent) => {
                                    setItemChange(item.key)
                                }}
                                style={{
                                    height: '100%',
                                }}
                                items={items2}
                            />
                        </Sider>
                        <Content
                            style={{
                                padding: '0 24px',
                                minHeight: 280,
                            }}
                        >
                            {/* <Title level={3}>{items2.filter((item) => item.key === itemChange)[0].label}</Title> */}
                            {/* {itemChange === 'home' && <Home />} */}
                            {itemChange === 'nft' && <Nft />}
                            {itemChange === 'previlege' && <Privileges />}
                            {itemChange === 'setting' && <Setting />}
                            {itemChange === 'guide' && (
                                <Button
                                    onClick={() => {
                                        let webStoreUrl = `${process.env.REACT_APP_URL}sbxg_admin_guide.pdf`
                                        window.open(webStoreUrl, '_blank')
                                    }}
                                >
                                    Go to guide
                                </Button>
                            )}
                        </Content>
                    </Layout>
                </Content>
            ) : (
                <>
                    <NoRight />
                </>
            )}

            <Footer
                style={{
                    textAlign: 'center',
                }}
            ></Footer>
        </Layout>
    )
}
export default App
