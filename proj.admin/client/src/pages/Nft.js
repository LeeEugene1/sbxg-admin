import React, { useEffect, useState, useContext } from 'react'
import { getWithAuth, postWithAuth, warning, randomNumber } from '../js/common'
import { Space, Table, Upload, Image, Input, loading, Button, Modal, Typography, message } from 'antd'
import { DownloadOutlined, UploadOutlined, InboxOutlined } from '@ant-design/icons'
import NFTDetail from './NFTDetail'
import ContentHeader from '../components/ContentHeader'
import ModalContext from '../context/ModalContext'

const { Column } = Table
const { Search } = Input
const { Text } = Typography

function Nft() {
    const { isModalOpen, showModal, closeModal } = useContext(ModalContext)
    const [isButtonLoading, setIsButtonLoading] = useState(false)
    const [fileList, setFileList] = useState([])

    const [data, setData] = useState({
        listData: [],
        totalPage: 0,
    })
    const [dataDetail, setDataDetail] = useState({})
    const [loading, setLoading] = useState(true)
    const [page, setPage] = useState(1)
    const [sort, setSort] = useState('token_id')
    const [isDesc, isSetDesc] = useState(true)
    const [search, setSearch] = useState('')
    const [isDetail, setIsDetail] = useState(false) //디테일, 뒤로가기
    const [isAdmin, SetIsAdmin] = useState(false)
    useEffect(() => {
        getDataList()
    }, [page, sort, isDesc, isDetail]) //최초실행, 관련 state 바뀔때마다 재실행

    const getDataList = async () => {
        setLoading(true)
        const randomNo = await randomNumber()
        await getWithAuth(
            `${process.env.REACT_APP_URL}nft/list?page=${
                page !== 0 ? page - 1 : page
            }&search=${search}&sortby=${sort}&orderby=${isDesc ? 'desc' : 'asc'}`
        )
            .then((res) => {
                if (res.message) {
                    throw new Error(res.message)
                }
                const totalPage = res.total
                const pageSize = res.num_per_page
                const initData = res.list.map((it, index) => {
                    return {
                        key: index,
                        idx: it.idx,
                        token_id: it.token_id,
                        holder: it.holder,
                        discord_id: it.discord_id,
                        nft_count: it.nft_count,
                        discord_name: it.discord_name,
                        grade: it.attributes.filter((e) => e.trait_type === 'Grade')[0].value,
                        exp: it.attributes.filter((e) => e.trait_type === 'EXP')[0].value,
                        artwork: `${it.artwork}?v=${randomNo}`,
                        attributes: it.attributes,
                    }
                })
                setData({ listData: initData, totalPage, pageSize })
                SetIsAdmin(res.isRight)
            })
            .catch((err) => {
                warning(err.message)
            })
            .finally(() => {
                setLoading(false)
            })
    }

    const handleSort = (by) => {
        if (by === sort) {
            isSetDesc(!isDesc)
        } else {
            isSetDesc(false) //초기값
        }
        setSort(by)
        // console.log('sort click!', by, isDesc)
    }

    const handleSearch = (value) => {
        setSearch(value)
        setPage(1)
        getDataList()
    }

    const handleEXPUpload = async () => {
        if (fileList.length === 0) {
            return false
        }
        setIsButtonLoading(true)
        const { jwt_token } = JSON.parse(sessionStorage.getItem('auth'))
        let file = fileList[0].originFileObj
        let formD = new FormData()
        formD.append('file', file)
        const option = {
            method: 'post',
            headers: {
                Authorization: jwt_token,
            },
            body: formD,
        }
        await fetch(`${process.env.REACT_APP_URL}batch/upload/exp`, option)
            .then((res) => res.json())
            .then((data) => {
                if (data.message) {
                    throw Error(data.message)
                }
                if (data.result) {
                    handleModalClose()
                    message.info('정상적으로 반영되었습니다.')
                }
            })
            .catch((err) => {
                try {
                    let str = ''
                    const isArr = JSON.parse(err.message)
                    isArr.map((e) => {
                        str += e.path.split('.')[0]
                    })
                    warning(`${str}번째 줄에 문제가 있습니다.`)
                } catch (exception) {
                    //json이 아닌경우
                    if (err.message.includes('token_id')) {
                        warning('NFT 목록에 없는 token_id가 있습니다. 다시 시도해주세요.')
                    } else if (err.message.includes('exp')) {
                        warning('NFT 목록에 없는 exp가 있습니다. 다시 시도해주세요.')
                    } else {
                        warning(err.message)
                    }
                }
                console.log(err)
            })
            .finally(() => {
                setFileList([])
                setIsButtonLoading(false)
            })
    }

    const handleExport = async () => {
        await getWithAuth(`${process.env.REACT_APP_URL}nft/export/code`)
            .then((res) => {
                if (res.message) {
                    throw new Error(res.message)
                }
                if (res.code) {
                    let url = `${process.env.REACT_APP_URL}nft/export/csv/${res.code}`
                    window.open(url, '_blank')
                }
            })
            .catch((err) => {
                warning(err.message)
            })
    }

    const toggleDetailPage = () => {
        setIsDetail(!isDetail)
    }

    const getDataDetail = async (tokenId) => {
        setLoading(true)
        const randomNo = await randomNumber()
        await getWithAuth(`${process.env.REACT_APP_URL}nft/list/details/${tokenId}`)
            .then((res) => JSON.parse(res))
            .then((res) => {
                if (res.message) {
                    throw new Error(res.message)
                }
                let newData = res
                newData.isAdmin = isAdmin
                newData.image = `${newData.image}?v=${randomNo}`
                setDataDetail(newData)
            })
            .then(() => {
                setIsDetail(true)
                setLoading(false)
            })
            .catch((err) => {
                warning(err.message)
                setLoading(false)
            })
    }

    const deleteAttribute = (value) => {
        let newArr = [...dataDetail.attributes]
        setDataDetail({
            ...dataDetail,
            attributes: newArr.filter((e) => e.trait_type !== value),
        })
    }

    const handleChange = (e) => {
        setSearch(e.target.value)
    }

    const handleFileChange = (info) => {
        //artwork
        let fileList = [...info.fileList]

        //csv 파일만 허용
        fileList = fileList.filter((file) => {
            const allowedExtensions = ['csv']
            const extension = file.name.split('.').pop().toLowerCase()
            return allowedExtensions.includes(extension)
        })

        // 동시에 2개 이상의 파일 업로드 불가능
        fileList = fileList.slice(-1)

        setFileList(fileList)
    }

    /**
     * 파일리스트 초기화후 모달창 닫기
     */
    const handleModalClose = () => {
        setFileList([])
        closeModal() //contextAPI
    }
    const { Dragger } = Upload
    return (
        <>
            {isDetail ? (
                <NFTDetail
                    toggleDetailPage={toggleDetailPage}
                    getDataDetail={getDataDetail}
                    dataDetail={dataDetail}
                    deleteAttribute={deleteAttribute}
                />
            ) : (
                <div>
                    <ContentHeader title={'NFT'} />
                    <p>NFT 관리페이지입니다. 세부페이지를 편집하려면 nft_manage 권한이 필요합니다.</p>
                    <div className="searchWrap">
                        <Space.Compact>
                            <Search
                                style={{ width: '300px' }}
                                placeholder="NFT id, Discord Id, Name or Holder"
                                onSearch={handleSearch}
                                allowClear
                                value={search}
                                onChange={handleChange}
                            />
                        </Space.Compact>
                        <div className="buttonWrap">
                            <Button
                                type="default"
                                shape="round"
                                onClick={() => showModal('')}
                                icon={<UploadOutlined />}
                                disabled={!isAdmin}
                            >
                                EXP Upload
                            </Button>
                            <Button type="default" shape="round" onClick={handleExport} icon={<DownloadOutlined />}>
                                NFT Export
                            </Button>
                        </div>
                    </div>
                    <Table //https://ant.design/components/table#Table
                        bordered
                        dataSource={data.listData}
                        scroll={{ x: 1200 }}
                        loading={loading}
                        pagination={{
                            current: page,
                            total: data.totalPage,
                            pageSize: data.pageSize,
                            showSizeChanger: false,
                            onChange: (page, sorter) => {
                                setPage(page)
                                console.log('table change event', sorter)
                            },
                        }}
                        onRow={(record, rowIndex) => {
                            return {
                                onClick: (e) => {
                                    if (e.target.tagName === 'TD') {
                                        getDataDetail(record.token_id)
                                    }
                                },
                            }
                        }}
                    >
                        <Column title="Index" dataIndex="idx" key="idx" />
                        <Column
                            title="Token_id"
                            dataIndex="token_id"
                            key="token_id"
                            sorter={() => {
                                handleSort('token_id')
                            }}
                        />
                        <Column title="Discord Id" dataIndex="discord_id" key="discord_id" />
                        <Column title="Discord Name" dataIndex="discord_name" key="discord_name" />
                        <Column title="Holder" dataIndex="holder" key="holder" />
                        <Column
                            title="NFT quantity"
                            dataIndex="nft_count"
                            key="nft_count"
                            sorter={() => {
                                handleSort('nft_count')
                            }}
                        />
                        <Column
                            title="Artwork"
                            dataIndex="artwork"
                            key="artwork"
                            render={(
                                i //data.listData.image
                            ) => (
                                <>
                                    <Image width={50} src={i} />
                                </>
                            )}
                        />
                        <Column
                            title="EXP"
                            dataIndex="exp"
                            key="exp"
                            sorter={() => {
                                handleSort('exp')
                            }}
                        />
                        <Column title="Grade" dataIndex="grade" key="grade" />
                    </Table>
                    <Modal
                        title={'EXP Upload'}
                        open={isModalOpen}
                        onCancel={handleModalClose}
                        footer={
                            <div className="buttonWrap">
                                <Button
                                    type="primary"
                                    loading={isButtonLoading}
                                    htmlType="submit"
                                    onClick={handleEXPUpload}
                                >
                                    Upload
                                </Button>
                                <Button type="default" onClick={handleModalClose}>
                                    Cancel
                                </Button>
                            </div>
                        }
                    >
                        <p>EXP에 따라 Grade, Artwork가 자동 변경됩니다.</p>

                        <Dragger
                            multiple={false}
                            beforeUpload={() => false}
                            onChange={handleFileChange}
                            fileList={fileList}
                            accept=".csv"
                            // customRequest={customRequest}
                            showUploadList={{ showRemoveIcon: false }}
                            // defaultFileList={fileList}
                        >
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">csv 파일로 1개까지만 업로드 가능합니다.</p>
                        </Dragger>
                        <p></p>
                        <Text type="secondary">* token_id, exp을 반드시 포함합니디.(아래 예시 참고)</Text>
                        <p></p>
                        <Table
                            columns={[
                                {
                                    title: 'token_id',
                                    dataIndex: 'token_id',
                                    key: 'token_id',
                                    width: 100,
                                },
                                {
                                    title: 'exp',
                                    dataIndex: 'exp',
                                    key: 'exp',
                                    width: 100,
                                },
                            ]}
                            dataSource={[
                                {
                                    key: '1',
                                    token_id: 1,
                                    exp: 15,
                                },
                                {
                                    key: '2',
                                    token_id: 2,
                                    exp: 25,
                                },
                            ]}
                            pagination={false}
                        />
                    </Modal>
                </div>
            )}
        </>
    )
}

export default Nft
