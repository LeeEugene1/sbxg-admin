import React, { useContext, useEffect, useState } from 'react'
import {
    PlusOutlined,
    LeftOutlined,
    EditOutlined,
    SettingOutlined,
    MinusCircleOutlined,
    ReloadOutlined,
} from '@ant-design/icons'
import { Button, Form, Input, Upload, Modal, Image, Space, message, Typography } from 'antd'
import { InboxOutlined, DeleteOutlined, UploadOutlined } from '@ant-design/icons'
import { getWithAuth, postWithAuth, warning, showConfirm } from '../js/common'
import ModalContext from '../context/ModalContext'
const { Text } = Typography

function NFTDetail({ toggleDetailPage, dataDetail, getDataDetail, deleteAttribute }) {
    const { currentModal, isModalOpen, showModal, closeModal } = useContext(ModalContext)
    const { token_id, exp, isAdmin, attributes, fixed_attributes } = dataDetail
    const [expValue, setExpValue] = useState(exp)
    const [isButtonLoading, setIsButtonLoading] = useState(false)

    /**
     * 하위 컴포넌트
     */
    const [fileList, setFileList] = useState([])
    const [propertyList, setPropertyList] = useState([])
    const [fixedAttributesList, setFixedAttributesList] = useState([])
    const [form] = Form.useForm()
    useEffect(() => {
        debugger;
        console.log(dataDetail)
        setExpValue(exp)

        let attributes_data = attributes.map((e) => {
            if (!e.display_type) {
                e.display_type = isNaN(parseFloat(e.value)) ? '' : 'number'
            }
            return e
        })

        let fixed_attributes_data = fixed_attributes.map((e) => {
            if (!e.display_type) {
                e.display_type = isNaN(parseFloat(e.value)) ? '' : 'number'
            }
            return e
        })
        debugger
        setPropertyList(attributes_data)
        setFixedAttributesList(fixed_attributes_data)
    }, [dataDetail]) //dataDetail바뀔때마다 리랜더링

    /**
     * API 재호출, 모달닫기
     */
    const updateData = () => {
        getDataDetail(token_id)
        handleModalCancel()
    }
    const handlePropertySubmit = async (values) => {
        let updatedPropertyList = [...propertyList]

        //데이터 타입 업데이트
        if (values.users) {
            values.users.map((e) => {
                let currentKey = e.key.toLowerCase()
                let findObj = updatedPropertyList.find((e) => e.trait_type.toLowerCase() === currentKey)
                if (findObj) {
                    findObj.value = e.value
                } else {
                    //key를 trait_type로 대체
                    e.trait_type = e.key
                    delete e.key
                    e.display_type = isNaN(parseFloat(e.value)) ? '' : 'number'
                    e.value = isNaN(parseFloat(e.value)) ? e.value : parseFloat(e.value)
                    updatedPropertyList.push(e)
                }
            })
        }

        await postWithAuth(
            `${process.env.REACT_APP_URL}nft/upload/property`,
            {
                token_id,
                property: updatedPropertyList,
            },
            'post'
        )
            .then((data) => {
                if (data.message) {
                    throw Error(data.message)
                }
                if (data.result) {
                    updateData()
                    message.info('정상적으로 반영되었습니다.')
                }
            })
            .catch((err) => {
                warning(err.message)
                console.log(err)
            })
    }
    const handlePropertyRemove = async (value) => {
        try {
            await showConfirm('정말로 삭제하시겠습니까? Submit 버튼을 누르면 정상 반영됩니다.')
        } catch (error) {
            return false
        }
        deleteAttribute(value)
    }
    const handleEXPUpdate = async () => {
        console.log('Received values of form:', expValue)
        await postWithAuth(
            `${process.env.REACT_APP_URL}nft/upload/exp`,
            {
                token_id,
                exp: expValue,
            },
            'post'
        )
            .then((data) => {
                if (data.message) {
                    throw new Error(data.message)
                }
                if (data.result) {
                    updateData()
                    message.info('정상적으로 반영되었습니다.')
                }
            })
            .catch((err) => {
                warning(err.message)
                console.log(err)
            })
    }
    const handleEXPChange = (e) => {
        setExpValue(e.target.value)
    }
    const handleArtworkUpload = async () => {
        if (fileList.length === 0) {
            return false
        }
        setIsButtonLoading(true)
        const { jwt_token } = JSON.parse(sessionStorage.getItem('auth'))
        let file = fileList[0].originFileObj
        let formD = new FormData()
        formD.append('token_id', token_id)
        formD.append('file', file)
        const option = {
            method: 'post',
            headers: {
                Authorization: jwt_token,
            },
            body: formD,
        }
        await fetch(`${process.env.REACT_APP_URL}nft/upload/image`, option)
            .then((res) => res.json())
            .then((data) => {
                if (data.message) {
                    throw Error(data.message)
                }
                if (data.result) {
                    updateData()
                    message.info('정상적으로 반영되었습니다.')
                }
                console.log(data)
            })
            .catch((err) => {
                warning(err.message)
                console.log(err)
            })
            .finally(() => {
                setIsButtonLoading(false)
            })
    }
    const handleArtworkDelete = async () => {
        setIsButtonLoading(true)
        await postWithAuth(
            `${process.env.REACT_APP_URL}nft/upload/image`,
            {
                token_id,
            },
            'DELETE'
        )
            .then((data) => {
                if (data.message) {
                    throw new Error(data.message)
                }
                console.log(data)
                if (data.result) {
                    updateData()
                    message.info('정상적으로 반영되었습니다.')
                }
            })
            .catch((err) => {
                console.log(err)
                if (err.message.includes('registered')) {
                    warning('등록된 Artwork가 없습니다.')
                } else {
                    warning(err.message)
                }
            })
            .finally(() => {
                setIsButtonLoading(false)
            })
    }
    const handleFileChange = (info) => {
        //artwork
        let fileList = [...info.fileList]

        //이미지 파일만 허용
        fileList = fileList.filter((file) => {
            const allowedExtensions = ['png', 'jpeg', 'jpg']
            const extension = file.name.split('.').pop().toLowerCase()
            return allowedExtensions.includes(extension)
        })

        // 동시에 2개 이상의 파일 업로드 불가능
        fileList = fileList.slice(-1)

        setFileList(fileList)
    }

    const drawModalFooter = (modalName) => {
        if (modalName === 'Artwork upload') {
            return (
                <div className="buttonWrap">
                    <Button type="primary" loading={isButtonLoading} htmlType="submit" onClick={handleArtworkUpload}>
                        Upload
                    </Button>
                    <Button type="default" onClick={handleModalCancel}>
                        Cancel
                    </Button>
                </div>
            )
        } else if (modalName === 'EXP') {
            return (
                <div className="buttonWrap">
                    <Button type="primary" htmlType="submit" form="expForm">
                        Update
                    </Button>
                    <Button type="default" onClick={handleModalCancel}>
                        Cancel
                    </Button>
                </div>
            )
        } else if (modalName === 'Property') {
            return (
                <div className="buttonWrap">
                    <Button type="primary" htmlType="submit" form="propertyForm">
                        Submit
                    </Button>
                    <Button type="default" onClick={handleModalCancel}>
                        Cancel
                    </Button>
                </div>
            )
        } else if (modalName === 'Artwork auto') {
            return (
                <div className="buttonWrap">
                    <Button type="primary" loading={isButtonLoading} htmlType="submit" onClick={handleArtworkDelete}>
                        Update
                    </Button>
                    <Button type="default" onClick={handleModalCancel}>
                        Cancel
                    </Button>
                </div>
            )
        }
    }

    const handleModalShow = (modalName) => {
        drawModalFooter(modalName)
        showModal(modalName) //conectAPI
    }

    const handleModalCancel = () => {
        const resetMapping = {
            Artwork: () => setFileList([]),
            EXP: () => setExpValue(exp),
            Property: () => form.resetFields(),
        }
        resetMapping[currentModal]?.()
        closeModal() //contextAPI
    }

    return (
        <div>
            <Button type="link" onClick={toggleDetailPage} style={{ paddingLeft: 0, color: '#F2BD27' }}>
                <LeftOutlined />
                Go back
            </Button>
            <Form
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 14,
                }}
                layout="horizontal"
                style={
                    {
                        // maxWidth: 600,
                    }
                }
            >
                <div className="formWrap">
                    <p>상세페이지</p>
                    {Object.keys(dataDetail).map((e) => {
                        if (e !== 'is_reg_image' && e !== 'attributes' && e !== 'isAdmin' && e !== 'fixed_attributes') {
                            if (e === 'image') {
                                return (
                                    <Form.Item label="Artwork">
                                        <Image width={100} src={dataDetail[e]} style={{ borderRadius: '10px' }} />
                                    </Form.Item>
                                )
                            } else {
                                return (
                                    <Form.Item label={e[0].toUpperCase() + e.substring(1)}>
                                        <Input value={dataDetail[e]} disabled />
                                    </Form.Item>
                                )
                            }
                        }
                    })}
                    {fixedAttributesList.length > 0 ? <p>고정 속성값</p> : ''}
                    {fixedAttributesList.length > 0 && fixedAttributesList.map(({ trait_type, value }) => {
                        return (
                            <Form.Item label={trait_type}>
                                <Input value={value} disabled />
                            </Form.Item>
                        )
                    })}
                    {propertyList.length > 0 ? <p>추가된 속성값</p> : ''}
                    {propertyList.length > 0 && propertyList.map(({ trait_type, value }) => {
                        debugger
                        return (
                            <Form.Item label={trait_type}>
                                <Input value={value} disabled />
                            </Form.Item>
                        )
                    })}
                </div>
                <div className="buttonWrap marginTop">
                    <Form.Item>
                        <Button
                            onClick={() => handleModalShow('Artwork auto')}
                            loading={isButtonLoading}
                            icon={<ReloadOutlined />}
                            disabled={!isAdmin}
                        >
                            Artwork 자동변경
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <Button
                            disabled={!isAdmin}
                            onClick={() => handleModalShow('Artwork upload')}
                            icon={<UploadOutlined />}
                        >
                            Artwork 수동변경
                        </Button>
                    </Form.Item>
                </div>
                <div className="buttonWrap">
                    <Form.Item>
                        <Button disabled={!isAdmin} onClick={() => handleModalShow('EXP')} icon={<EditOutlined />}>
                            EXP
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <Button
                            disabled={!isAdmin}
                            onClick={() => handleModalShow('Property')}
                            icon={<SettingOutlined />}
                        >
                            Property
                        </Button>
                    </Form.Item>
                </div>
            </Form>
            <Modal
                title={currentModal}
                open={isModalOpen}
                onCancel={handleModalCancel}
                footer={drawModalFooter(currentModal)}
            >
                {currentModal === 'Artwork upload' && (
                    <ArtworkComponent fileList={fileList} handleFileChange={handleFileChange} />
                )}
                {currentModal === 'EXP' && (
                    <EXPComponent
                        expValue={expValue}
                        handleEXPUpdate={handleEXPUpdate}
                        handleEXPChange={handleEXPChange}
                    />
                )}
                {currentModal === 'Property' && (
                    <PropertyComponent
                        propertyList={propertyList}
                        handlePropertySubmit={handlePropertySubmit}
                        handlePropertyRemove={handlePropertyRemove}
                        form={form}
                    />
                )}
                {currentModal === 'Artwork auto' && <div>Artwork 자동변경시 Grade에 맞는 이미지로 업데이트됩니다.</div>}
            </Modal>
        </div>
    )
}

const ArtworkComponent = ({ fileList, handleFileChange }) => {
    const { Dragger } = Upload

    return (
        <>
            <Dragger
                multiple={false}
                listType="picture"
                beforeUpload={() => false}
                onChange={handleFileChange}
                fileList={fileList}
                accept=".png, .jpeg, .jpg"
                // customRequest={customRequest}
                showUploadList={{ showRemoveIcon: false }}
                // defaultFileList={fileList}
            >
                <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                </p>
                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                <p className="ant-upload-hint">이미지타입의 파일로 1개까지만 업로드 가능합니다.</p>
            </Dragger>
        </>
    )
}

const PropertyComponent = ({ propertyList, handlePropertySubmit, handlePropertyRemove, form }) => {
    const onFinish = (values) => {
        handlePropertySubmit(values)
    }

    return (
        <Form
            form={form}
            name="dynamic_form_nest_item"
            onFinish={onFinish}
            style={{
                maxWidth: 600,
            }}
            autoComplete="off"
            id="propertyForm"
        >
            <p>추가된 Property는 NFT 상세페이지에서만 확인가능합니다.</p>
            <Text type="secondary"> * 'exp', 'meta', 'grade’ 이름의 프로퍼티는 적용되지않습니다.</Text>
            <p></p>
            <Form.List name="users">
                {(fields, { add, remove }) => (
                    <>
                        {propertyList.map(({ key, value, trait_type }, idx) => {
                            return (
                                <Space
                                    key={idx}
                                    style={{
                                        display: 'flex',
                                        marginBottom: 8,
                                    }}
                                    align="baseline"
                                >
                                    <Form.Item>
                                        <Input value={trait_type} disabled />
                                    </Form.Item>
                                    <Form.Item>
                                        <Input value={value} disabled />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => handlePropertyRemove(trait_type)} />
                                </Space>
                            )
                        })}
                        {fields.map(({ key, name, ...restField }) => (
                            <Space
                                key={key}
                                style={{
                                    display: 'flex',
                                    marginBottom: 8,
                                }}
                                align="baseline"
                            >
                                <Form.Item
                                    {...restField}
                                    name={[name, 'key']}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Missing property Name',
                                        },
                                    ]}
                                >
                                    <Input placeholder="Property Name" />
                                </Form.Item>
                                <Form.Item
                                    {...restField}
                                    name={[name, 'value']}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Missing property value',
                                        },
                                    ]}
                                >
                                    <Input placeholder="Property value" />
                                </Form.Item>
                                <MinusCircleOutlined onClick={() => remove(name)} />
                            </Space>
                        ))}
                        <Form.Item>
                            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                Add property
                            </Button>
                        </Form.Item>
                    </>
                )}
            </Form.List>
        </Form>
    )
}

const EXPComponent = ({ expValue, handleEXPChange, handleEXPUpdate }) => {
    const onFinish = () => {
        handleEXPUpdate()
    }

    return (
        <>
            <p>EXP 변경시 Grade가 업데이트됩니다.</p>

            <Form
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 14,
                }}
                layout="horizontal"
                style={
                    {
                        // maxWidth: 600,
                    }
                }
                onFinish={onFinish}
                id="expForm"
            >
                <div className="formWrap">
                    <Form.Item label="EXP">
                        <Input value={expValue} name={'inputExp'} onChange={handleEXPChange} />
                    </Form.Item>
                </div>
            </Form>
        </>
    )
}

export default NFTDetail
