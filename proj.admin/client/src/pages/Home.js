import React from 'react'
import ContentHeader from '../components/ContentHeader'

function Home() {
    return (
        <div>
            <ContentHeader title={'Home'} />
        </div>
    )
}

export default Home
