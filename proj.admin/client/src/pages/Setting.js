import React, { useEffect, useState } from 'react'
import { Table, Input, InputNumber, Popconfirm, Form, Button, message } from 'antd'
import ContentHeader from '../components/ContentHeader'
import { getWithAuth, postWithAuth, warning, Message, showConfirm } from '../js/common'
import { EditOutlined, CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons'
// 10: Bronze, 20: Silver, 30:Gold
function Setting() {
    const [isAdmin, SetIsAdmin] = useState(false)
    const [dataGrade, setGrade] = useState([])
    const [initData, setInitData] = useState([])
    const [isEdit, setIsEdit] = useState(false)
    const [form] = Form.useForm()
    const [isButtonLoading, setIsButtonLoading] = useState(false)

    const getEXPGrade = async () => {
        await getWithAuth(`${process.env.REACT_APP_URL}setting/exp`)
            .then((res) => {
                const { config, isRight } = res

                let orderby = config.sort((a, b) => b.key - a.key)
                setGrade(orderby)
                setInitData(orderby)
                SetIsAdmin(isRight)
            })
            .catch((err) => {
                warning(err.message)
            })
    }
    const setEXPGrade = async () => {
        let updatedDataSource = dataGrade

        const gold = parseInt(dataGrade.find((e) => e.key === '30')['to'])
        const silver = parseInt(dataGrade.find((e) => e.key === '20')['to'])
        const bronze = parseInt(dataGrade.find((e) => e.key === '10')['to'])
        if (!gold > 0 || !silver > 0 || !bronze > 0) {
            warning(`빈칸을 채워주세요.(숫자만 허용)`)
            return false
        }
        if (gold <= silver || gold <= bronze || silver <= bronze) {
            warning(`값을 올바르게 입력해주세요.(Gold > Silver > Bronze)`)
            return false
        }
        try {
            await showConfirm('전체 반영까지 1분이상 소요됩니다. 정말로 반영하시겠습니까?')
        } catch (error) {
            return false
        }
        setIsButtonLoading(true)
        SetIsAdmin(false)
        try {
            await postWithAuth(`${process.env.REACT_APP_URL}batch/setting/exp`, updatedDataSource, 'PUT')
                .then((data) => {
                    if (data.result) {
                        handleToggleEdit()
                        setGrade(updatedDataSource)
                        message.info('정상적으로 반영되었습니다.')
                    }
                })
                .catch((err) => {
                    console.log(err)
                    throw new Error(err.message)
                })
                .finally(() => {
                    setIsButtonLoading(false)
                    SetIsAdmin(true)
                })
        } catch (err) {
            setIsButtonLoading(false)
            warning(`에러가 발생하여 전체 반영하지못하였습니다. (${err.message})`)
        }
    }

    const handleToggleEdit = () => {
        setIsEdit(!isEdit)
    }

    useEffect(() => {
        getEXPGrade()
    }, [])

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (_, record) => {
                if (isEdit) {
                    return (
                        <Form.Item>
                            <Input value={record.name} disabled />
                        </Form.Item>
                    )
                } else {
                    return <p>{record.name}</p>
                }
            },
        },
        {
            title: 'Range',
            dataIndex: 'to',
            render: (_, record) => {
                if (isEdit) {
                    return (
                        <Form.Item>
                            <Input
                                name={record.name}
                                type="number"
                                value={record.to}
                                onChange={(e) => {
                                    const { name, value } = e.target
                                    //얕은복사
                                    // let newData = [...dataGrade]
                                    // newData.find((i) => i.name === name)['to'] = value

                                    //깊은복사
                                    const newData = dataGrade.map((item) =>
                                        item.name === name ? { ...item, to: value } : item
                                    )

                                    setGrade(newData)
                                }}
                            />
                        </Form.Item>
                    )
                } else {
                    if (record.key === '10') {
                        return <p>0 ~ {record.to}</p>
                    } else {
                        return <p>~ {record.to}</p>
                    }
                }
            },
        },
    ]
    return (
        <div>
            <ContentHeader title={'Setting'} />
            <p>기타 관리페이지 입니다. 편집하려면 setting_manage 권한이 필요합니다.</p>
            <h1>등급표</h1>
            <Form form={form}>
                <Table columns={columns} dataSource={dataGrade} pagination={false} responsive="sm"></Table>
                {!isEdit ? (
                    <div className="buttonWrap marginTop right">
                        <Button
                            type="default"
                            shape="round"
                            icon={<EditOutlined />}
                            disabled={!isAdmin}
                            onClick={handleToggleEdit}
                        >
                            Edit
                        </Button>
                    </div>
                ) : (
                    <div className="buttonWrap marginTop right">
                        <Button
                            type="default"
                            shape="round"
                            icon={<CloseCircleOutlined />}
                            disabled={!isAdmin}
                            onClick={() => {
                                setGrade(initData)
                                setIsEdit(!isEdit)
                            }}
                        >
                            Cancel
                        </Button>
                        <Button
                            type="primary"
                            shape="round"
                            icon={<CheckCircleOutlined />}
                            disabled={!isAdmin}
                            onClick={setEXPGrade}
                            loading={isButtonLoading}
                        >
                            Save
                        </Button>
                    </div>
                )}
            </Form>
        </div>
    )
}

export default Setting
