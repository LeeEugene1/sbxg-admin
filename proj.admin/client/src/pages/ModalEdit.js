import React, { useEffect } from 'react'
import { Space, Table, Tag, Button, Modal, Form, Input, Checkbox, Avatar, Card, Skeleton, Col, Row } from 'antd'

function ModalEdit({ open, loading, view, onUpdate, handleCancel, handleInputChange, handleCheckboxChange }) {
    useEffect(() => {
        console.log('Modal edit render test')
    })
    return (
        <div>
            <Modal
                open={open}
                title={view.title}
                key={'privilegesModal'}
                onOk={onUpdate}
                onCancel={handleCancel}
                footer={[
                    <Button key="submit" type="primary" loading={loading} onClick={onUpdate}>
                        {view.title}
                    </Button>,
                ]}
            >
                <Form
                    labelCol={{
                        span: 4,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    layout="horizontal"
                    style={{
                        maxWidth: 600,
                    }}
                >
                    <Form.Item label="Name">
                        <Input
                            key={'nameI'}
                            name={'name'}
                            onChange={(e) => {
                                // setView({ ...view, name: e.target.value })
                                handleInputChange(e)
                            }}
                            value={view.name}
                        />
                    </Form.Item>
                    <Form.Item label="Address">
                        <Input
                            key={'addrI'}
                            name={'address'}
                            onChange={(e) => {
                                // setView({ ...view, address: e.target.value })
                                handleInputChange(e)
                            }}
                            value={view.address}
                        />
                    </Form.Item>
                    <Form.Item label="Rights">
                        <Checkbox.Group
                            key={'CheckBoxG'}
                            options={view.rights}
                            onChange={(item) => {
                                // setView({ ...view, arrChcked: item })
                                handleCheckboxChange(item)
                            }}
                            value={view.arrChcked}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

export default ModalEdit
