import React, { useEffect, useState, useReducer, useCallback } from 'react'
import {
    Space,
    Table,
    Tag,
    Button,
    Modal,
    Form,
    Input,
    Checkbox,
    Avatar,
    Card,
    Skeleton,
    Col,
    Row,
    message,
} from 'antd'
import { getWithAuth, postWithAuth, warning, showConfirm } from '../js/common'
import { UserAddOutlined, EditOutlined, EllipsisOutlined, SettingOutlined, DeleteOutlined } from '@ant-design/icons'
import ModalEdit from './ModalEdit'
import NoRight from '../components/NoRight'
import ContentHeader from '../components/ContentHeader'
const { Meta } = Card

function Privileges() {
    /******************************************************
     * Hook
     *******************************************************/
    const [isRight, SetIsRight] = useState(true)
    const [view, setView] = React.useState({
        name: '',
        address: '',
        rights: [],
        title: 'Edit',
    })
    const [initialRights, setInitialRights] = useState([])

    const [loading, setLoading] = useState(true)

    const [open, setOpen] = useState(false)

    useEffect(() => {
        getDataList()
    }, [])

    useEffect(() => {}, [view])

    /******************************************************
     * Init
     *******************************************************/
    const reducer = (state, action) => {
        switch (action.type) {
            case 'INIT': {
                return action.data
            }
            case 'DELETE': {
                return state.filter((it) => it.address !== action.addr)
            }
            default:
                return state
        }
    }
    const [data, dispatch] = useReducer(reducer, [])

    const getDataList = async () => {
        setLoading(true) //start loading
        await getWithAuth(`${process.env.REACT_APP_URL}previlege/list`)
            .then((res) => {
                console.log(res)
                if (res.code === 401) {
                    throw Error('no right')
                }
                const initData = res.wallets.map((it, idx) => {
                    return {
                        key: idx,
                        id: it.id,
                        name: it.name,
                        address: it.address,
                        rights: it.rights,
                    }
                })
                let rightArr = res.ALL_RIGHTS //['', '', '']
                let mappedRights = rightArr.map((e) => {
                    let obj = {}
                    obj.label = e
                    obj.value = e
                    return obj
                })

                setInitialRights(mappedRights)
                dispatch({ type: 'INIT', data: initData })
            })
            .catch((err) => {
                SetIsRight(false)
                if (!err.message.includes('right')) {
                    warning(err.message)
                }
            })
            .finally(() => {
                setLoading(false)
            })
    }

    const showModal = (addr = '') => {
        let filteredData = data.filter((it) => it.address === addr)

        let arrChcked = []
        let arrRights = filteredData[0]?.rights.map((item) => {
            arrChcked.push(item?.valid === true ? item.right : '')
            return {
                label: item.right,
                value: item.right,
            }
        })

        setView({
            name: filteredData[0]?.name || '',
            address: filteredData[0]?.address || '',
            rights: arrRights || initialRights,
            arrChcked: arrChcked,
            title: addr !== '' ? 'Edit' : 'Create',
        })

        setOpen(true)
    }

    /******************************************************
     * Event
     *******************************************************/
    const onUpdate = async () => {
        //create, update
        setLoading(true) //start loading
        console.log('view', view)
        if (view.name === '' || view.address === '') {
            warning('값을 모두 채워주세요.')
            setLoading(false)
            return false
        }
        let filteredArray = view.arrChcked.filter((item) => item !== '') //빈 문자열 제거
        await postWithAuth(
            `${process.env.REACT_APP_URL}previlege/setup`,
            {
                name: view.name,
                wallet: view.address,
                rights: filteredArray,
            },
            'put'
        )
            .then((res) => {
                if (res.message) {
                    throw Error(res.message)
                }
                if (res.result) {
                    getDataList()
                    message.info('정상적으로 반영되었습니다.')
                }
            })
            .catch((err) => {
                if (err.message.includes('right')) {
                    warning('권한이 없습니다.')
                } else if (err.message.includes('invalid')) {
                    warning('값이 유효하지 않습니다. 지갑주소가 유효한지 확인해주세요.')
                } else {
                    warning(err.message)
                }
            })
            .finally(() => {
                handleCancel()
            })
    }

    const handleCancel = () => {
        setLoading(false) //stop loading
        setOpen(false) //close modal
    }

    const handleInputChange = (e) => {
        setView({
            ...view,
            [e.target.name]: e.target.value,
        })
    }

    const handleCheckboxChange = (item) => {
        setView({ ...view, arrChcked: item })
    }

    const onRemove = useCallback(async (addr) => {
        try {
            await showConfirm(`지갑주소: ${addr}을 정말로 삭제하시겠습니까?`)
        } catch (error) {
            console.log(error.message)
            return false
        }

        await postWithAuth(
            `${process.env.REACT_APP_URL}previlege/setup`,
            {
                wallet: addr,
            },
            'delete'
        )
            .then((res) => {
                if (res.message) {
                    throw Error(res.message)
                }
                if (res.result) {
                    dispatch({ type: 'DELETE', addr })
                    message.info('정상적으로 반영되었습니다.')
                }
            })
            .catch((err) => {
                if (err.message.includes('right')) {
                    warning('삭제 권한이 없습니다.')
                } else {
                    warning(err.message)
                }
            })
    }, [])

    /******************************************************
     * jsx
     *******************************************************/
    return (
        <>
            {isRight ? (
                <div>
                    <ContentHeader title={'Privilege'} />
                    <p>
                        권한 관리페이지입니다. 리스트에 등록된 계정은 현재 메뉴를 제외한 모든 메뉴에 접근이 가능합니다.
                    </p>
                    <p>편집 권한을 주려면 메뉴명_manage를 등록합니다.</p>
                    <div className="searchWrap">
                        <Button type="primary" shape="round" onClick={() => showModal()} icon={<UserAddOutlined />}>
                            Create
                        </Button>
                    </div>
                    <div className="cardWrap">
                        {data.map((e, index) => {
                            return (
                                <Card
                                    style={{
                                        width: 300,
                                        marginTop: 16,
                                    }}
                                    actions={[
                                        <EditOutlined key="edit" onClick={() => showModal(e.address)} />,
                                        <DeleteOutlined key="delete" onClick={() => onRemove(e.address)} />,
                                    ]}
                                    key={index}
                                >
                                    <Skeleton loading={loading} avatar active>
                                        <Meta
                                            avatar={
                                                <Avatar
                                                    src={`https://xsgames.co/randomusers/avatar.php?g=pixel&key=${e.address.slice(
                                                        -1
                                                    )}`}
                                                />
                                            }
                                            style={{
                                                height: 100,
                                            }}
                                            title={`${e.name}(...${e.address.slice(-4)})`}
                                            description={e.rights
                                                .filter((e) => e.valid === true)
                                                .map((x, idx) => (
                                                    <Tag color="gold" key={x + idx}>
                                                        {x.right}
                                                    </Tag>
                                                ))}
                                        />
                                    </Skeleton>
                                </Card>
                            )
                        })}
                    </div>
                    <ModalEdit
                        open={open}
                        loading={loading}
                        view={view}
                        onUpdate={onUpdate}
                        handleCancel={handleCancel}
                        handleInputChange={handleInputChange}
                        handleCheckboxChange={handleCheckboxChange}
                    />
                </div>
            ) : (
                <>
                    <NoRight />
                </>
            )}
        </>
    )
}

export default Privileges
