import { ConfigProvider } from 'antd'
import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import './style/common.scss'

import App from './App'
import reportWebVitals from './reportWebVitals'

import { ModalProvider } from './context/ModalContext'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <ModalProvider>
        <ConfigProvider theme={{ token: { colorPrimary: '#F2BD27' } }}>
            {/* <React.StrictMode> */}
            <App />
            {/* </React.StrictMode> */}
        </ConfigProvider>
    </ModalProvider>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
