import React, { createContext, useState } from 'react'
import { Button } from 'antd'

const ModalContext = createContext()

export const ModalProvider = ({ children }) => {
    const [currentModal, setCurrentModal] = useState('artwork')
    const [isModalOpen, setIsModalOpen] = useState(false)

    const showModal = (modalName) => {
        setCurrentModal(modalName)
        setIsModalOpen(true)
    }

    const closeModal = () => {
        setIsModalOpen(false)
    }

    const modalContextValue = {
        currentModal,
        isModalOpen,
        showModal,
        closeModal,
    }

    return <ModalContext.Provider value={modalContextValue}>{children}</ModalContext.Provider>
}

export default ModalContext
