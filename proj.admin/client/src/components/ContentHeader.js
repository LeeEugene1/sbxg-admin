import React from 'react'
import { Typography } from 'antd'
const { Title } = Typography

export default function ContentHeader({ title }) {
    return <Title level={3}>{title}</Title>
}
