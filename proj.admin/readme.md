## 프론트 배포
server - index.js에 client에서 빌드된 정적 파일을 서빙하는 구조

1. client에서 stage배포 또는 production 빌드 명령어 실행
qa용 빌드 정적 파일
```npm run build-qa```

배포용 빌드 정적 파일
```npm run build-prod```

프론트에서 preview 혹은 서버에서 배포된내용 확인 예:http://localhost:5002/connect/1

2. client내 빌드된 폴더(build-qa | build-prod)를 server내로 이동
