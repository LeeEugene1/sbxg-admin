const express = require("express");
const cors = require("cors");
const app = express();
const config = require("./config");
const path = require("path");
const cookieParser = require("cookie-parser");

const build_folder = config.server.POLYGON_NETWORK == config.networks.MUMBAI? 'build-qa' : 'build' ;

app.use(cors());
app.use(cookieParser());
app.use(express.static(build_folder)); //마음대로 꺼내써도된다.허용, use(): URL에 상관없이 앱이 요청을 수신할 때마다 매번 실행
// app.use(express.static('public'))
// app.use(express.static(path.join(__dirname, './build')));
// app.use(express.static(path.join(__dirname, './public')));

app.use(express.json({ limit: "50mb" }));
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  // res.sendFile(path.join(__dirname, './index.html'));
  // res.end('heeeeee')
  try {
    // res.send('index.html')
    res.sendFile(__dirname + `/${build_folder}/index.html`);
  } catch (error) {
    console.log(error);
  }
});

app.use("/auth/", require("./router/auth.js"));
app.use("/nft/", require("./router/nft.js")); // nft 라우트를 추가하고, 기본 경로로 /nft 사용
app.use("/meta/", require("./router/meta.js")); // contract에 저장되는 tokenUri용 (거래소 호출) || 그냥 nft사용?
app.use("/batch/", require("./router/batch.js")); 
app.use("/setting/", require("./router/setting.js")); 
app.use("/previlege/", require("./router/previlege.js"));

let port = config.server.PORT;
app.listen(port, () => {
  console.log(`BUILD_FOLDER: ${build_folder}`);
  console.log(`The Express server is listening at PORT: ${port}`);
});

module.exports = app; // for testing
