const express = require('express');
const sbxg = require('../libs/sbxg');
const { validationResult, param } = require('express-validator');
const {db} = require("../libs/db");

let router = express.Router();

/**
 * Opensea Metadata
 *  */
router.get("/:token_id",
param("token_id").exists().isInt(),
async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        return res.status(400).json({ message: "invalid params", errors: errors.array() });
    }

    const token_id = parseInt(req.params.token_id);

    try{

        if(token_id === 6000){
            return res.json({"name": "UNAVAILABLE", "image": "https://cdn.sbxg.win/img/unavailable.png", "attributes": [], "description": ""});
        }

        const result = await db.query(`SELECT * FROM TB_NFT WHERE token_id=?`,[token_id]);
        if(result.length === 0){
            return res.json({"name":null,"image":null,"description":null,"attributes":null});
        }

        let data = result[0];
        let meta_edit_attrs = data.meta_edit_attrs || [];
        let meta = data.meta;
        if(typeof meta === 'string'){
            meta = JSON.parse(meta);
        }
        if(typeof meta_edit_attrs === 'string'){
            meta_edit_attrs = JSON.parse(meta_edit_attrs);
        }
        
        meta['attributes'] = meta['attributes']
                            .concat(meta_edit_attrs)
                            .concat([{
                                "trait_type": "Grade",
                                "value": sbxg.getGradeName(parseInt(data.grade))
                            },{
                                "trait_type": "EXP",
                                "value": Number(data.exp),
                                "display_type": "number"
                            }]);

        return res.json(meta);

    }catch(error){
        console.log(error);
        return res.json({"name":null,"image":null,"description":null,"attributes":null});
    }
    
});

module.exports = router;