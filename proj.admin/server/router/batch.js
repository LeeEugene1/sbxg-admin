
const express = require('express');
const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');
const auth = require("../libs/auth");
const sbxg = require('../libs/sbxg');
const {db, assert_changed_row} = require("../libs/db");
const { validationResult, param, check, body } = require('express-validator');
let router = express.Router();

// timeout error고려, polling형식으로 함
let batchProcessingStatus = {
    'uploadExp':{ isProcessing: false, error: null },
    'syncGrade':{ isProcessing: false, error: null }
  };

router.get('/status/:api', 
auth.checkJwt(),
param("api").exists().notEmpty().isIn(['uploadExp','syncGrade']),
async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }
    try{
        const api_name = req.params.api;
        return res.json(JSON.stringify(batchProcessingStatus[api_name]));

    }catch(error){
        console.log(error.message);
        return res.status(400).json({ message: error.message });
    }

});

router.post("/upload/exp", 
auth.checkJwt(),
auth.haveRights([auth.RIGHTS_NFT]),
async(req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }
    
    if(!!batchProcessingStatus['uploadExp']['isProcessing']){
        return json.status(400).json({ message: 'exp batch is in processing'});
    }

    const transaction = await db.beginTransaction();
    try{

        await sbxg.uploadCsv(req, res);
        if (!req.file) {
            throw Error('Failed to upload the CSV file.');
        }
        
        const tmp_file_path = path.join(__dirname,'..',`/tmp/${req.file.filename}`);
        let csv_data = fs.readFileSync(tmp_file_path, 'utf8').replace(/(\r\n)*$/, '');
        const parsedData = papaparse.parse(csv_data, { header: true });
        const csv_rows = parsedData.data;
        fs.unlink(tmp_file_path, (err)=>{if(err) console.log(err)});

        /**
         *  [ ]  운영툴에서 관리자가 CSV를 업로드해서 개별 NFT의 EXP값 일괄 수정 가능
            [ ]  EXP 값에 따른 Grade 변경 & Grade 값에 따른 Artwork 변경은 자동으로 반영
         */

        let valid_objs = { 'body': csv_rows }
        await check('*.token_id').exists().notEmpty().isNumeric().withMessage(`invalid token_id`).run(valid_objs);
        await check('*.exp').exists().notEmpty().isNumeric().withMessage(`invalid exp`).run(valid_objs);
        const error = validationResult(valid_objs);
        if (!error.isEmpty()) {
            throw Error(JSON.stringify(error.errors));
        }

        batchProcessingStatus['uploadExp']['isProcessing'] = true;
        let {exp_range} = await transaction.one(`SELECT \`value\` AS exp_range FROM TB_CONFIG WHERE \`key\`='exp_range'`);
        if(typeof exp_range == 'string'){
            exp_range = JSON.parse(exp_range);
        }

        let split_size = 1000; 
        const split_csv_rows = [];
        for (let i = 0; i < csv_rows.length; i += split_size) {
          const chunk = csv_rows.slice(i, i + split_size);
          split_csv_rows.push(chunk);
        }
        
        for (let i = 0; i < split_csv_rows.length; i++) {
            let rows= split_csv_rows[i]; //array
            let db_data = await transaction.query(`SELECT token_id, grade, meta FROM TB_NFT WHERE token_id IN (?) AND token_id!=6000`, [rows.map((r)=>parseInt(r.token_id))]);
            if(rows.length != db_data.length){
                throw Error('invalid token_id');
            }

            for(const row of rows) {
                let _token_id = parseInt(row['token_id']);
                let _exp = parseInt(row['exp']);
                let _row_meta = db_data.filter((d)=>parseInt(d.token_id) === parseInt(_token_id))[0];
                let org_image = _row_meta['meta']['image'];
                let org_grade = parseInt(_row_meta['grade']);

                // grade
                let _finalized_grade = sbxg.getGradeFromExp(exp_range, _exp);
                if(org_grade != _finalized_grade){
                    // meta by grade
                    const response = await fetch(`https://cdn.sbxg.win/meta/${_finalized_grade}/${_token_id}.json`);
                    if (!response.ok) {
                        throw new Error(`Network response was not ok, token id: ${_token_id}`);
                    }
                    let _meta = await response.json();
                    if(typeof _meta === 'string'){
                        _meta = JSON.parse(_meta);
                    }

                    // image
                    const is_registered = sbxg.isImageRegistered(org_image);
                    if(!is_registered){
                        _meta['image'] = sbxg.getDefaultImageUrl(_finalized_grade, _token_id);
                    }else{
                        _meta['image'] = org_image;
                    }

                    await transaction.query(`UPDATE TB_NFT SET exp=?, grade=?, meta=? WHERE token_id=?`, [_exp, _finalized_grade, JSON.stringify(_meta), _token_id]);

                }else{
                    await transaction.query(`UPDATE TB_NFT SET exp=? WHERE token_id=?`, [_exp, _token_id]);

                }

            }

        }

        await transaction.commit();
        batchProcessingStatus['uploadExp']['error'] = null;
        return res.json({ result: true });

    }catch(error){
        console.log(error.message);
        await transaction.rollback();
        batchProcessingStatus['uploadExp']['error'] = error.message;
        return res.status(400).json({ message: error.message });

    }finally{
        batchProcessingStatus['uploadExp']['isProcessing'] = false;
    }

});


router.put("/setting/exp",
  auth.checkJwt(),
  auth.haveRights([auth.RIGHTS_SETTING]),
  body("*.to").exists().notEmpty().isInt({ min: 1 }),
  body("*.key").exists().notEmpty().isIn([10,20,30]),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "invalid param", errors: errors.array() });
    }

    if(batchProcessingStatus['syncGrade']['isProcessing']){
        return json.status(400).json({success: false, message: 'batch is in processing'});
    }

    batchProcessingStatus['syncGrade']['isProcessing'] = true;

    const transaction = await db.beginTransaction();
    try {
        let rawData = req.body;
        const convertedObj = {};
        rawData.forEach((c) => {
            convertedObj[c.key] = {
            to: parseInt(c.to),
            };
        });

        const sortedKeys = Object.keys(convertedObj).sort();
        const sortedObj = {};
        sortedKeys.forEach((key) => {
            sortedObj[key] = convertedObj[key];
        });

        let prev_to = parseInt(sortedObj["10"]["to"])-1;
        for (let grade in sortedObj) {
            let to = parseInt(sortedObj[grade]["to"]);
            if (!(to > prev_to)) {
            throw Error(`${sbxg.getGradeName(grade)} must be bigger than ${prev_to}`);
            }
            sortedObj[grade]["to"] = to;
            prev_to = to;
        }

        await transaction.query( `UPDATE TB_CONFIG SET  \`value\` = ? WHERE \`key\`= 'exp_range'`, [JSON.stringify(sortedObj)]);

        // batch 처리
        const exp_range = sortedObj;
        let page = 0;
        const num_per_page = 1000;

        while(1){
            const start = page * num_per_page;
            const nfts = await transaction.query(`SELECT * FROM TB_NFT WHERE token_id != 6000 ORDER BY token_id ASC LIMIT ?,?`, [start,num_per_page]);
            if(nfts.length == 0) break;
            
            for(const nft of nfts){ 
                let _grade = sbxg.getGradeFromExp(exp_range, nft['exp']);
                let _token_id = parseInt(nft['token_id']);
                let _org_grade = parseInt(nft['grade']);
                let _org_image = nft['meta']['image'];

                if(_org_grade != _grade){
                    // meta by grade
                    const response = await fetch(`https://cdn.sbxg.win/meta/${_grade}/${_token_id}.json`);
                    if (!response.ok) {
                        throw new Error(`Network response was not ok, token id: ${_token_id}`);
                    }
                    let _meta = await response.json();
                    if(typeof _meta === 'string'){
                        _meta = JSON.parse(_meta);
                    }
                    
                    if(!sbxg.isImageRegistered(_org_image)){
                        _meta['image'] = sbxg.getDefaultImageUrl(_grade, _token_id);
                    }else{
                        _meta['image'] = _org_image;
                    }

                    await transaction.query(`UPDATE TB_NFT SET grade=?, meta=? WHERE token_id=?`,[_grade, JSON.stringify(_meta), _token_id]);
                    
                }

            }
            page++;
        }
        await transaction.commit();
        batchProcessingStatus['syncGrade']['error'] = null;
        return res.json({ result: true });

    }catch(error){
        console.log(error.message);
        if (transaction) await transaction.rollback();
        batchProcessingStatus['syncGrade']['error'] = error.message;
        return res.status(500).json({ message: error.message });

    }finally{
        batchProcessingStatus['syncGrade']['isProcessing'] = false;
    }

  }
);



// router.post('/sync-grade', 
// auth.checkJwt(),
// auth.haveRights([auth.RIGHTS_SETTING]),
// async(req, res)=>{

//     if(batchProcessingStatus['syncGrade']['isProcessing']){
//         return json.status(400).json({success: false, message: 'batch is in processing'});
//     }
//     console.log(new Date());//TODO 삭제
//     const transaction = await db.beginTransaction();
//     try{
//         batchProcessingStatus['syncGrade']['isProcessing'] = true;
//         console.log(batchProcessingStatus['syncGrade']['isProcessing']);//TODO 삭제

//         const { exp_range } = await transaction.one(`SELECT \`value\` AS exp_range FROM TB_CONFIG WHERE \`key\`= 'exp_range'`);

//         let page = 0;
//         const num_per_page = 1000;
//         while(1){
//             const start = page * num_per_page;
//             const nfts = await transaction.query(`SELECT * FROM TB_NFT WHERE token_id != 6000 ORDER BY token_id ASC LIMIT ?,?`, [start,num_per_page]);
//             if(nfts.length == 0) break;
            
//             let _grade = 0;
//             for(const nft of nfts){ 
//                 _grade = sbxg.getGradeFromExp(exp_range, nft['exp']);
                
//                 if(!sbxg.isImageRegistered(nft['meta']['image'])){
//                     nft['meta']['image'] = sbxg.getDefaultImageUrl(_grade, nft['token_id']);
//                 }

//                 await transaction.query(`UPDATE TB_NFT SET grade=?, meta=? WHERE token_id=?`,[_grade, JSON.stringify(nft['meta']), nft['token_id']]);
//             }
//             page++;
//         }
//         await transaction.commit();
//         return res.json({ result: true });

//     }catch(error){
//         console.log(error.message);
//         if (transaction) await transaction.rollback();
//         return res.status(500).json({ message: error.message });
//     }finally{
//         batchProcessingStatus['syncGrade']['isProcessing'] = false;
//         console.log(batchProcessingStatus['syncGrade']['isProcessing']);//TODO 삭제
//         console.log(new Date());//TODO 삭제
//     }

// })




//  /** TODO수정
//  * {property:[{trait_type:'', value:'', display_type:''},{trait_type:'', value:'', display_type:''}...]}
// */
// router.put("/property",
// auth.checkJwt(),
// auth.haveRights([auth.RIGHTS_NFT]),
// body("trait_type").exists().isString(),
// body("value").exists(),
// body("display_type").optional().isIn(['number']),
// async(req, res)=>{
// const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         return res.status(400).json({ message: "invalid param", errors: errors.array() });
//     }
//     /**{ 7월 작업
//         "trait_type": "Grade",
//         "value": sbxg.getGradeName(parseInt(data.grade))
//     },{
//         "trait_type": "EXP",
//         "value": Number(data.exp),
//         "display_type": "number"
//     }*/
//     const transaction = await db.beginTransaction();
//     try{

//         let trait_type = sanitizeHtml(req.body.trait_type);
//         const display_type = req.body.display_type || '';
//         let value = display_type === 'number'? Number(req.body.value) : sanitizeHtml(req.body.value);

//         // validate trait_type : only string(english), _, space 
//         let ex = /^[A-Za-z]+((\s|_)*[A-Za-z])*$/i;
//         if(!ex.test(trait_type)){
//             throw Error('unavailable trait_type');
//         }

//         trait_type = trait_type.replace(/\b[a-z]/, type => type.toUpperCase());
//         value = display_type === 'number'? value : value.replace(/\b[a-z]/, v => v.toUpperCase());
//         // console.log(trait_type, value, display_type);

//         //unavailable: exp,meta,grade
//         if(['exp','meta','grade'].includes(trait_type.toLowerCase())){
//             throw Error('unavailable trait_type');
//         }

//         // duplicate
//         const {attributes} = await transaction.one(`SELECT meta->'$.attributes' AS attributes FROM TB_NFT LIMIT 1`);
//         let filterd_trait = attributes.filter((attr)=>attr.trait_type.toLowerCase() == trait_type.toLowerCase());
//         if(filterd_trait.length>0){
//             throw Error('duplicated trait_type');
//         }

//         let nft_meta = await transaction.query(`SELECT token_id, meta FROM TB_NFT`);
//         for(const nft of nft_meta){
//             const _attribute = display_type === 'number'? {trait_type, value, display_type} : {trait_type, value};
//             nft.meta.attributes.push(_attribute);
//             let result = await transaction.query(`UPDATE TB_NFT SET meta=? WHERE token_id=?`,[JSON.stringify(nft.meta), nft.token_id]);
//             assert_changed_row(result,1);
//         }

//         await transaction.commit();

//         return res.json({result: true});
        
//     }catch(error){
//         console.log(error);
//         await transaction.rollback();
//         return res.json({message: error.message});
//     }

// });

// router.delete("/property", 
// auth.checkJwt(),
// auth.haveRights([auth.RIGHTS_NFT]),
// body("trait_type").exists().isString(),
// async(req,res)=>{
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         return res.status(400).json({ message: "invalid param", errors: errors.array() });
//     }

//     const trait_type = req.body.trait_type.toLowerCase();

//     const transaction = await db.beginTransaction();
//     try{

//         //check exist
//         const {attributes} = await transaction.one(`SELECT meta->'$.attributes' AS attributes FROM TB_NFT LIMIT 1`);
//         let filterd_trait = attributes.filter((attr)=>attr.trait_type.toLowerCase() == trait_type);
//         if(filterd_trait.length === 0){
//             throw Error('not exist trait_type');
//         }

//         // delete
//         let nft_meta = await transaction.query(`SELECT token_id, meta FROM TB_NFT`);
//         for(const nft of nft_meta){
//             let trait_index = nft.meta.attributes.findIndex((el)=>el.trait_type.toLowerCase() === trait_type)
//             nft.meta.attributes.splice(trait_index,1);
//             let result = await transaction.query(`UPDATE TB_NFT SET meta=? WHERE token_id=?`,[JSON.stringify(nft.meta), nft.token_id]);
//             assert_changed_row(result,1);
//         }

//         await transaction.commit();

//         return res.json({result: true});
        
//     }catch(error){
//         console.log(error);
//         await transaction.rollback();
//         return res.json({message: error.message});
//     }

// })


module.exports=router;