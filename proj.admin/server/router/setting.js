const express = require("express");
const auth = require("../libs/auth");
const sbxg = require("../libs/sbxg");
const { db, assert_changed_row } = require("../libs/db");
const { validationResult, body } = require("express-validator");
let router = express.Router();

router.get("/exp", auth.checkJwt(), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(400)
      .json({ message: "invalid param", errors: errors.array() });
  }

  let isRight;
  try {
    isRight = auth.isRights([auth.RIGHTS_SETTING])(req, res);
  } catch (e) {
    console.log(e.message);
    isRight = false;
  }

  try {
    const { exp_range } = await db.one(
      `SELECT \`value\` AS exp_range FROM TB_CONFIG WHERE \`key\`= 'exp_range'`
    );

    let config = [];
    for (const key in exp_range) {
      exp_range[key]["key"] = key;
      exp_range[key]["name"] = sbxg.getGradeName(key);
      config.push(exp_range[key]);
    }

    return res.json({ config, isRight: isRight });
  } catch (error) {
    console.log(error.message);
    return res.status(400).json({ message: error.message });
  }
});

router.put(
  "/exp",
  auth.checkJwt(),
  auth.haveRights([auth.RIGHTS_SETTING]),
  body("*.to").exists().notEmpty().isInt({ min: 1 }),
  body("*.key").exists().notEmpty().isIn([10,20,30]),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "invalid param", errors: errors.array() });
    }

    try {
      let rawData = req.body;
      const convertedObj = {};
      rawData.forEach((c) => {
        convertedObj[c.key] = {
          to: parseInt(c.to),
        };
      });

      const sortedKeys = Object.keys(convertedObj).sort();
      const sortedObj = {};
      sortedKeys.forEach((key) => {
        sortedObj[key] = convertedObj[key];
      });

      let prev_to = parseInt(sortedObj["10"]["to"])-1;
      for (let grade in sortedObj) {
        let to = parseInt(sortedObj[grade]["to"]);
        if (!(to > prev_to)) {
          throw Error(`${sbxg.getGradeName(grade)} must be bigger than ${prev_to}`);
        }
        sortedObj[grade]["to"] = to;
        prev_to = to;
      }

      await db.query( `UPDATE TB_CONFIG SET  \`value\` = ? WHERE \`key\`= 'exp_range'`, [JSON.stringify(sortedObj)]);

      return res.json({ result: true });

    } catch (error) {
      console.log(error.message);
      return res.status(400).json({ message: error.message });
    }
  }
);

module.exports = router;
