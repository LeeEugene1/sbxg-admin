const express = require("express");
const { db } = require("../libs/db");
const sbxg = require("../libs/sbxg");
const auth = require("../libs/auth");
const sanitizeHtml = require("sanitize-html");
const { validationResult, body } = require("express-validator");
let router = express.Router();

router.get("/list",
auth.checkJwt(),
auth.haveRights(["previleges_manage"]),
async (req, res) => {

    try {
      let row = await db.query("SELECT `value` FROM TB_CONFIG WHERE `key`='admin'");
      let w = row[0]["value"];
      let ALL_RIGHTS = auth.ALL_RIGHTS;

      let wallets=[];
      for(const addr in w){
        w[addr].rights = ALL_RIGHTS.map((r) => {
          let found = w[addr].rights.indexOf(r) != -1;
          return { right: r, valid: found };
        });
        w[addr].id = w[addr].id;
        w[addr].address = addr;
        if(!sbxg.isOwner(addr)){
              wallets.push(w[addr]);
        }
      }

      res.status(200).json({ wallets, ALL_RIGHTS });

    } catch (error) {
      console.log(error);
      return res.status(400).json({ message: error.message });
    }
  }
);

router.put("/setup",
  auth.checkJwt(),
  auth.haveRights([auth.RIGHTS_PREVILEGES]),
  body("wallet").exists().isEthereumAddress(),
  body("rights").exists().isArray(),
  body("name").exists().isString(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "invalid param", errors: errors.array() });
    }

    const wallet = req.body.wallet;
    const rights = req.body.rights;
    const name = sanitizeHtml(req.body.name);

    if(!!sbxg.isOwner(wallet)){
      return res.status(400).json({ message: 'cannot set'});
    }

    try {
      let { config } = await db.one(
        `SELECT \`value\` AS config FROM TB_CONFIG WHERE \`key\`='admin'`
      );
      if (typeof config === "string") {
        config = JSON.parse(config);
      }

      //validate right
      for (const right of rights) {
        if (!auth.ALL_RIGHTS.includes(right)) {
          throw Error("unavailable right");
        }
      }

      const cf_wallets = Object.keys(config).map((cf) => cf.toLowerCase());
      if (cf_wallets.includes(wallet.toLowerCase())) {
        // update
        config[wallet]["name"] = name;
        config[wallet]["rights"] = rights;
      } else {
        // insert
        config[wallet] = {
          id: Math.floor(Math.random() * 8999)+1000,
          name: name,
          rights: rights,
        };
      }

      await db.query(`UPDATE TB_CONFIG SET \`value\`=? WHERE \`key\`='admin'`, [
        JSON.stringify(config),
      ]);

      return res.json({ result: true });
    } catch (error) {
      console.log(error);
      return res.status(400).json({ message: error.message });
    }
  }
);

router.delete("/setup",
  auth.checkJwt(),
  auth.haveRights([auth.RIGHTS_PREVILEGES]),
  body("wallet").exists().isEthereumAddress(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }

    const wallet = req.body.wallet;

    if(!!sbxg.isOwner(wallet)){
      return res.status(400).json({ message: 'cannot delete'});
    }

    try {
      let { config } = await db.one(`SELECT \`value\` AS config FROM TB_CONFIG WHERE \`key\`='admin'`);
      if (typeof config === "string") {
        config = JSON.parse(config);
      }

      const cf_wallets = Object.keys(config).map((cf) => cf.toLowerCase());
      if (cf_wallets.includes(wallet.toLowerCase())) {
        delete config[wallet];
        await db.query(`UPDATE TB_CONFIG SET \`value\`=? WHERE \`key\`='admin'`, [JSON.stringify(config)]);
      } else {
        throw Error("not exist wallet");
      }

      return res.json({ result: true });
    } catch (error) {
      console.log(error);
      return res.status(400).json({ message: error.message });
    }
  }
);

module.exports = router;
