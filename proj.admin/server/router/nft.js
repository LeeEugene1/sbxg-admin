const express = require('express');
const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');
const sanitizeHtml = require("sanitize-html");
const auth = require("../libs/auth");
const sbxg = require('../libs/sbxg');
const {db, assert_changed_row} = require("../libs/db");
const { encrypt, decrypt } = require("../libs/common");
const cloudflare = require(path.join(__dirname, '../../../', './lib.common/cloudflare'));
const { validationResult, body, query, param, check } = require('express-validator');
let router = express.Router();

router.get("/list", 
auth.checkJwt(),
query("page").exists(),
query("num_per_page").optional(),
query("search").optional().exists(),
query("sortby").optional().exists().isIn(['nft_count','exp','token_id']),
query("orderby").optional().exists().isIn(['desc','asc']),
async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }

    let isRight;
    try {
      isRight = auth.isRights([auth.RIGHTS_NFT])(req, res);
    } catch (e) {
      console.log(e.message);
      isRight = false;
    }

    const page = parseInt(req.query.page) || 0;
    const num_per_page = parseInt(req.query.num_per_page) || 10; 
    const start = page * num_per_page;
    
    let search = sanitizeHtml(req.query.search);
    let sortby = req.query.sortby || 'token_id';
    let orderby = req.query.orderby || 'desc';

    let data_mapping = [];
    let query_where = '1 ';

    if(!!search){
        query_where += `AND (token_id LIKE ? OR LOWER(n.holder) LIKE ? OR d.user_id LIKE ? OR d.name LIKE ?)`;
        data_mapping.push(...[`%${search}%`, `%${search.toLowerCase()}%`, `%${search}%`, `%${search.toLowerCase()}%`]);
    }

    data_mapping.push(...[start, num_per_page])

    const transaction = await db.beginTransaction();
    try{
        let list = await transaction.query(`SELECT SQL_CALC_FOUND_ROWS
                                                     b.*,
                                                     meta->>'$.image' AS artwork
                                             FROM (
                                                    SELECT @rownum:=@rownum+1 AS idx, 
                                                            token_id, 
                                                            \`exp\`, 
                                                            grade, 
                                                            meta,
                                                            n.holder,
                                                            nft_count,
                                                            IFNULL(d.user_id, 'NaN') AS discord_id,
                                                            d.name AS discord_name
                                                    FROM TB_NFT AS n 
                                                    INNER JOIN (SELECT holder, COUNT(token_id) AS nft_count FROM TB_NFT WHERE token_id!=6000 GROUP BY holder) AS hn ON hn.holder = n.holder
                                                    LEFT JOIN TB_DISCORD_USER AS d ON n.holder = d.wallet AND stage=3, (SELECT @rownum:=0) TMP 
                                                    WHERE token_id!=6000 AND ${query_where} 
                                             )AS b 
                                             ORDER BY ${sortby} ${orderby} LIMIT ?, ?` ,data_mapping);
        const { total } = await transaction.one("SELECT FOUND_ROWS() as total");

        await transaction.commit();

        list = list.map((row)=>{

            row.discord_name = row.discord_name? row.discord_name.replace(/#0\b/g, '') : null;
            let meta = row.meta;
            if(typeof meta === 'string'){
                meta = JSON.parse(meta);
            }
            
            meta.attributes = [{
                                    "trait_type": "Grade",
                                    "value": sbxg.getGradeName(parseInt(row.grade))
                                },{
                                    "trait_type": "EXP",
                                    "value": Number(row.exp) 
                                }].concat(meta.attributes);

            row['attributes'] = meta.attributes;

            delete row.exp;
            delete row.grade;
            delete row.meta;
            return row;
        });

        return res.json({list, total, num_per_page, isRight: isRight});

    }catch(error){
        console.log(error);
        await transaction.rollback();
        return res.status(400).json({ message: error.message });
    }
});


router.get("/list/details/:token_id",
auth.checkJwt(),
param("token_id").exists().isInt({min:1}),
async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }
    const token_id = req.params.token_id;

    try{

        const detail_data = await db.query(`SELECT token_id, holder, IFNULL(user_id, 'NaN') AS discord_id, d.name AS discord_name, exp, grade, meta, meta_edit_attrs
                                            FROM TB_NFT AS n LEFT JOIN TB_DISCORD_USER AS d ON n.holder=d.wallet WHERE token_id=?`, [token_id]);
        if(detail_data.length == 0){
            throw Error('not exist token');
        }
        
        detail_data.map((data)=>{
            data.discord_name = data.discord_name? data.discord_name.replace(/#0\b/g, '') : null;
            data.grade = sbxg.getGradeName(parseInt(data.grade));
            let meta = data.meta;
            if(typeof meta === 'string'){
                meta = JSON.parse(meta);
            }
            data['is_reg_image'] = sbxg.isImageRegistered(meta.image); // true이면 이미지 삭제 버튼 제공
            data['image'] = meta.image;
            data['fixed_attributes'] = meta.attributes;
            data['attributes'] = data.meta_edit_attrs || [];

            delete data.meta_edit_attrs;
            delete data.meta;
        })

        return res.json(JSON.stringify(detail_data[0]));

    }catch(error){
        console.log(error.message);    
        return res.status(400).json({ message: error.message });
    }

});

router.get("/export/code",
auth.checkJwt(),
async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }

    try{
        const key = new Date().toISOString().slice(0, 10);  // yyyy-mm-dd
        const encrypted_data = encrypt('exportabccsv', key);
        // console.log('암호화된 데이터:', key, encrypted_data);
        return res.json({code:encrypted_data})

    }catch(error){
        console.log(error.message);    
        return res.status(400).json({ message: error.message });
    }
   
});

router.get("/export/csv/:code",
param("code").exists().notEmpty(),
async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(`invalid params`);
    }
    const encrypted_data = req.params.code;
    const now_date = new Date().toISOString().slice(0, 10);

    try{
        const decrypted_data = decrypt(encrypted_data, now_date);
        // console.log('복호화된 데이터:', decrypted_data);

        if(decrypted_data!=='exportabccsv'){
            return res.status(400).json(`invalid access`);
        }

        const csv_data = await db.query(`SELECT token_id, holder, IFNULL(user_id, 'NaN') AS discord_id, name AS discord_name, grade, exp, meta 
                                         FROM TB_NFT LEFT JOIN TB_DISCORD_USER ON holder=wallet 
                                         WHERE token_id!=6000
                                        ORDER BY token_id ASC`);

        csv_data.map((data)=>{
            data.grade = sbxg.getGradeName(parseInt(data.grade));
            data.discord_name = data.discord_name? data.discord_name.replace(/#0\b/g, '') : null;
            // data.discord_id = !!data.discord_id? `${data.discord_id}`:'NaN';
            // data.discord_id = !!data.discord_id? `\'${data.discord_id}\'`:'NaN';
            let meta = data.meta;
            if(typeof meta === 'string'){
                meta = JSON.parse(meta);
            }
            for(let attr of meta.attributes){
                data[attr.trait_type.toLowerCase()] = attr.value;
            }
            meta.attributes = (meta.attributes).concat([{
                                    "trait_type": "Grade",
                                    "value": data.grade
                                },{
                                    "trait_type": "EXP",
                                    "value": Number(data.exp),
                                    "display_type": "number"
                                }]);
            delete data.meta;
            data['meta'] = JSON.stringify(meta);
        })

        // const now = new Date();
        // const filename = `${now.getFullYear()}-${('0' + (now.getMonth() + 1)).slice(-2)}-${('0' + now.getDate()).slice(-2)}.csv`;
        const filename = `${now_date}.csv`;

        const csv_option = {
            header: true, // 헤더 행 포함
            columns: Object.keys(csv_data[0]) // 컬럼 이름 지정
          };

        const file_path = path.join(__dirname, '..', filename);
        const csv = papaparse.unparse(csv_data, csv_option);
        fs.writeFile(file_path, csv, 'utf8', (err) => {
            if(err){ throw Error(err); }
            res.setHeader('Content-Disposition', `attachment; filename=${filename}`)
            res.setHeader('Content-Type', `text/csv`)
            res.download(file_path, ()=>{
                fs.unlink(file_path, (err)=>{if(err) console.log(err)});
            });

        });
    
    }catch(error){
        console.log(`export error\n${error}`);
        return res.status(400).json('cannot export data');
    }
    
});


router.post("/upload/image", 
auth.checkJwt(),
auth.haveRights([auth.RIGHTS_NFT]),
async(req, res)=>{

    const transaction = await db.beginTransaction();
    try {

        await sbxg.uploadNFTImage(req, res);
        await body('token_id').notEmpty().withMessage('Token ID is required').run(req);
    
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            console.log(errors);
            return res.status(400).json({ message: "invalid params", errors: errors.array() });
        }

        const token_id = req.body.token_id;
        const artwork = `https://${req.file.bucket}/${req.file.key}`;

        let {meta} = await transaction.one(`SELECT meta FROM TB_NFT WHERE token_id=?`,[token_id]);
        if(typeof meta == 'string'){
            meta = JSON.parse(meta);
        }
        meta['image'] = artwork;
        await transaction.query(`UPDATE TB_NFT SET meta=? WHERE token_id=?`,[JSON.stringify(meta), token_id]);
        await transaction.commit();
        await cloudflare.purgeCache([artwork]);
        return res.json({"result": true, "artwork":artwork});

    } catch (error) {
        await transaction.rollback();
        console.log(error.message);
        return res.status(400).json({"message": error.message});
    }
  
});

router.delete("/upload/image", 
auth.checkJwt(),
auth.haveRights([auth.RIGHTS_NFT]),
body("token_id").exists().notEmpty().isInt({min:1}),
async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }

    const transaction = await db.beginTransaction();
    try{
        const token_id = req.body.token_id;
        const token_data = await transaction.query(`SELECT token_id, grade, meta FROM TB_NFT WHERE token_id=?`, [token_id]); 
        if(token_data.length == 0){
            throw Error('not exist token');
        }

        let meta = token_data[0]['meta'];
        const isResistred = sbxg.isImageRegistered(meta.image);
        if(!isResistred){
            throw Error('not registered image.');
        }

        // 나중에 업로드시 덮어씌워짐
        // 처리 도중에 에러 발생 하면 삭제된 이미지가 보이지 않기 때문에 혹시 모르니 삭제X,  
        // await sbxg.deleteNFTImage(meta.image);

        //grade 의 artwork
        const grade = token_data[0]['grade'];
        const artwork = sbxg.getDefaultImageUrl(grade, token_id);
        meta.image = artwork;

        const result = await transaction.query(`UPDATE TB_NFT SET meta=? WHERE token_id=?`, [JSON.stringify(meta), token_id]);
        assert_changed_row(result,  1);

        await transaction.commit();

        return res.json({"result": true, "artwork":artwork});
        
    }catch(error){
        console.log(error);
        await transaction.rollback();
        return res.status(400).json({"message": error.message});
    }

});


router.post("/upload/exp", 
auth.checkJwt(),
auth.haveRights([auth.RIGHTS_NFT]),
body("token_id").exists().notEmpty(),
body("exp").exists().notEmpty().isInt({min:1}),
async(req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }

    const transaction = await db.beginTransaction();
    try{

        const token_id = parseInt(req.body.token_id);
        const exp = parseInt(req.body.exp);

        /**
        * exp에 따라 grade, artwork 변경하여 저장 + 아트워크 여부
        */
        let {exp_range} = await transaction.one(`SELECT \`value\` AS exp_range FROM TB_CONFIG WHERE \`key\`='exp_range'`);
        if(typeof exp_range == 'string' ){
            exp_range = JSON.parse(exp_range);
        }

        const {grade, meta} = await transaction.one(`SELECT grade, meta FROM TB_NFT WHERE token_id =?`, [token_id]);
        let finalized_grade = sbxg.getGradeFromExp(exp_range, exp);

        if(grade != finalized_grade){

            const response = await fetch(`https://cdn.sbxg.win/meta/${finalized_grade}/${token_id}.json`);
            if (!response.ok) {
                throw new Error(`Network response was not ok, token id: ${token_id}`);
            }
            let _meta = await response.json();
            if(typeof _meta === 'string'){
                _meta = JSON.parse(_meta);
            }
            
            if(!sbxg.isImageRegistered(meta.image)){
                _meta['image'] = sbxg.getDefaultImageUrl(finalized_grade, token_id);
            }else{
                _meta['image'] = meta.image;
            }
    
            await transaction.query(`UPDATE TB_NFT SET exp=?, grade=?, meta=? WHERE token_id=?`, [exp, finalized_grade, JSON.stringify(_meta), token_id]);
            
        }else{
            await transaction.query(`UPDATE TB_NFT SET exp=? WHERE token_id=?`, [exp, token_id]);

        }
           
        await transaction.commit();
        return res.json({ result: true });

    }catch(error){
        console.log(error.message);
        await transaction.rollback();
        return res.status(400).json({ message: error.message });
    }

});


router.post("/upload/property",
auth.checkJwt(),
auth.haveRights([auth.RIGHTS_NFT]),
body("token_id").exists().notEmpty(),
body("property.*.trait_type").optional().notEmpty().isString(),
body("property.*.value").optional().notEmpty(),
body("property.*.display_type").optional(),
async(req, res)=>{
const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ message: "invalid param", errors: errors.array() });
    }

    const transaction = await db.beginTransaction();
    try{

        const token_id = parseInt(req.body.token_id);
        const trait_list = req.body.property;
        
        let attributes = [];
        if(trait_list.length>0){
            for(const trait of trait_list){
                let trait_type = sanitizeHtml(trait['trait_type']);
                //unavailable: exp,meta,grade
                if(['exp','meta','grade'].includes(trait_type.toLowerCase())){
                    continue;
                }
                // validate trait_type : only string(english), ., _, space 
                let ex = /^[0-9A-Za-z]+((\s|_|.)*[0-9A-Za-z])*$/i;
                if(!ex.test(trait_type)){ 
                    throw Error('unavailable trait_type');
                }
                // uppercase
                trait_type = trait_type.replace(/(^|\s)\S/g, type => type.toUpperCase());
    
                let display_type = sanitizeHtml(trait['display_type']); //number일 경우
                let value = display_type === 'number'? Number(trait['value']) : trait['value'].replace(/\b[a-z]/, v => sanitizeHtml(v).toUpperCase());
    
                attributes.push(!!display_type?{trait_type, value, display_type}:{trait_type, value});
                // console.log(attributes);
            }
        }

        // let {meta} = await transaction.one(`SELECT meta, grade FROM TB_NFT WHERE token_id=?`, [token_id]);
        // if(typeof meta == 'string') meta=JSON.parse(meta);
        // meta['attributes']=attributes;
        await transaction.query(`UPDATE TB_NFT SET meta_edit_attrs=? WHERE token_id=?`,[JSON.stringify(attributes), token_id]);

        await transaction.commit();

        return res.json({result: true});
        
    }catch(error){
        console.log(error);
        await transaction.rollback();
        return res.status(400).json({message: error.message});
    }

});



module.exports = router;