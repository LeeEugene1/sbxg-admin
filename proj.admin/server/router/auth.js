const path = require("path");
const jwt = require("jsonwebtoken");
const config = require("../config");
const infura = require(path.join(
  __dirname,
  "../../../",
  "./lib.common/infura"
));
const { get_admin, insertAdmLog } = require("../libs/common");
const { validationResult, body } = require("express-validator");

const express = require("express");
let router = express.Router();

router.post(
  "/",
  body("address").isEthereumAddress(),
  body("signature").exists(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res
        .status(400)
        .json({ message: "invalid params", errors: errors.array() });

    const message = config.server.SIGN_MESSAGE;
    const address = req.body.address;
    const signature = req.body.signature;

    let web3 = infura.getWeb3(config.server.POLYGON_NETWORK);
    let hash = web3.utils.soliditySha3(message, address);
    const recovered = web3.eth.accounts.recover(hash, signature);

    let admin_info = await get_admin(address);
    if (admin_info == null) return res.status(400).json({ error: "not admin" });

    if (address.toLowerCase() !== recovered.toLowerCase()) {
      return res.status(500).json({
        message: "internal server error",
      });
    } else {
      //서명일치시 jwt발급
      const id = admin_info.id;
      const name = admin_info.name;
      const rights = admin_info.rights;

      const payload = { wallet: recovered, name, rights };
      const options = { expiresIn: "7d", issuer: "sheepfarm-auth-server" };
      const jwt_token = jwt.sign(payload, config.server.JWT_SECRET, options);

      const cookieOptions = {
        // sameSite: "strict",
        sameSite: "none",
        secure: true,
        httpOnly: true,
        // domain: "http://localhost",
        // expires: new Date(Date.now() + parseInt(ENV.AUTH_EXPIREATION)),
        // maxAge: ENV.AUTH_EXPIREATION,
        maxAge: 24 * 60 * 60 * 1000,
        path: "/",
      };

      res.cookie("x_auth", "tt3ttt", cookieOptions);

      // let user = await db.users.getUser(db.db, address);
      // if (user == null) {
      //   await db.users.createUser(db.db, address);
      // } else {
      //   await db.users.updateUserLoginAt(db.db, address);
      // }
      await insertAdmLog(id, "/auth", JSON.stringify(req.body));

      return res.status(200).json({
        message: "jwt token issued",
        jwt_token,
      });
    }
  }
);

module.exports = router;
