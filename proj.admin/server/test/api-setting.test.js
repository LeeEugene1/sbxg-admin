const chai = require('chai');
const { expect, assert } = chai;
const config = require('../config');
const server = require('../index');
const infura = require("../../../lib.common/infura");
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('[SETTING]', () => {
    
    const wallet_pk = "d74299f3cf148b0fd5a58abb9bc6c26f3d3e0e1bb17dac4600be2b84e6be5789";
    const wallet='0xF090f3401389b9b3d77871DCbe6e6330fD7D4278'; 
    let web3;
    let jwt;

    before('Prepare JWT', async () => {
        web3 = infura.getWeb3('mumbai');        
        const message = config.server.SIGN_MESSAGE;
        const hash = web3.utils.soliditySha3(message, wallet);
        const signObj = await web3.eth.accounts.sign(hash, wallet_pk);
        const res = await chai.request(server)
            .post("/auth")
            .send({
                "address": wallet,
                "wallet_type": "metamask",
                "signature": signObj['signature']
            })
        jwt = res.body.jwt_token;
        console.log("generated jwt:" + jwt);
    });

    it('GET /setting/exp', async()=>{
        let res = await chai.request(server)
        .get(`/setting/exp`)
        .set('Authorization', jwt);

        expect(res.status).to.equal(200);
        console.log(res.body);
    });

    it('PUT /setting/exp:: expect fail:: invalid param', async()=>{
        let res = await chai.request(server)
        .put(`/setting/exp`)
        .set('Authorization', jwt)
        .send([
            { to: '8d1', key: '10', name: 'Bronze' },
            { to: 43, key: '20', name: 'Silver' },
            { to: 81, key: '301', name: 'Gold' },
          ]);

        expect(res.status).to.equal(400);
        expect(res.body.message).to.include('invalid param');
    });

    it('PUT /setting/exp:: expect fail :: ${grade} must be bigger than ${prevTo}', async()=>{
        let res = await chai.request(server)
        .put(`/setting/exp`)
        .set('Authorization', jwt)
        .send([
            { to: 51, key: '10', name: 'Bronze' },
            { to: 43, key: '20', name: 'Silver' },
            { to: 31, key: '30', name: 'Gold' },
          ]);

        expect(res.status).to.equal(400);
        expect(res.body.message).to.include('must be bigger than');
    });

    it.only('PUT /batch/setting/exp', async()=>{
        let res = await chai.request(server)
        .put(`/batch/setting/exp`)
        .set('Authorization', jwt)
        .send([
            { to: 20, key: '10', name: 'Bronze' },
            { to: 51, key: '20', name: 'Silver' },
            { to: 91, key: '30', name: 'Gold' },
          ]);

        expect(res.status).to.equal(200);
        console.log(res.body);
    });

    it('POST /batch/sync-grade', async()=>{
         await chai.request(server)
        .post(`/batch/sync-grade`)
        .set('Authorization', jwt)

        setInterval(async() => {
            let res = await chai.request(server)
            .get(`/batch/status/syncGrade`)
            console.log(res.body);
        }, 2000);
    });


});