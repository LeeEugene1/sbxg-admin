const chai = require('chai');
const { expect, assert } = chai;
const config = require('../config');
const server = require('../index');
const infura = require("../../../lib.common/infura");
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('[MGZ NFT List]', () => {
    
    const wallet_pk = "d74299f3cf148b0fd5a58abb9bc6c26f3d3e0e1bb17dac4600be2b84e6be5789";
    const wallet='0xF090f3401389b9b3d77871DCbe6e6330fD7D4278'; 
    let web3;
    let jwt;
    jwt=`generated jwt:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ3YWxsZXQiOiIweEYwOTBmMzQwMTM4OWI5YjNkNzc4NzFEQ2JlNmU2MzMwZkQ3RDQyNzgiLCJuYW1lIjoiT1dORVIiLCJyaWdodHMiOlsicHJldmlsZWdlc19tYW5hZ2UiLCJuZnRfbWFuYWdlIiwic2V0dGluZ19tYW5hZ2UiXSwiaWF0IjoxNjg5MDU0MDgxLCJleHAiOjE2ODk2NTg4ODEsImlzcyI6InNoZWVwZmFybS1hdXRoLXNlcnZlciJ9.mLrVZFGgnJmsq6x1EaKtEOD9HybteV4AvuBbM_iRV-U`;

    before('Prepare JWT', async () => {
        web3 = infura.getWeb3('mumbai');        
        const message = config.server.SIGN_MESSAGE;
        const hash = web3.utils.soliditySha3(message, wallet);
        const signObj = await web3.eth.accounts.sign(hash, wallet_pk);
        const res = await chai.request(server)
            .post("/auth")
            .send({
                "address": wallet,
                "wallet_type": "metamask",
                "signature": signObj['signature']
            })
        jwt = res.body.jwt_token;
        console.log("generated jwt:" + jwt);
    });

    it('metadata', async()=>{
        let res = await chai.request(server)
        .get(`/meta/1`)
        console.log(res.body);
    });

    it('/nft/list', async ()=>{
        let res = await chai.request(server)
        .get(`/nft/list?page=0`)
        .set('Authorization', jwt);

        expect(res.status).to.equal(200);
        expect(res.body).has.property("list");
        console.log(res.body.length);
    })

    it('/nft/list/details/1', async ()=>{
        let res = await chai.request(server)
        .get(`/nft/list/details/1`)
        .set('Authorization', jwt);
        expect(res.status).to.equal(200);
        expect(res.body.length).to.equal(1);
        console.log(res.body);
    })
  
    it.only('Post /nft/upload/property: expect 200', async ()=>{
        let res = await chai.request(server)
        .post(`/nft/upload/property`)
        .send({"token_id":5999, 
               "property":[
                {'trait_type': 'grade', 'value': '1ss0', 'display_type':'number'},
                {'trait_type': 'test', 'value': 'value1', 'display_type':''}, 
                {'trait_type': 'speed', 'value': '2', 'display_type':'number'}]})
        .set('Authorization', jwt);
        expect(res.status).to.equal(200);
        expect(res.body).has.property("result");
        expect(res.body.result).to.equal(true);
    })

    it('Post /nft/upload/property: delete property: expect 200', async ()=>{
        let res = await chai.request(server)
        .post(`/nft/upload/property`)
        .send({"token_id":5999, 
               "property":[]})
        .set('Authorization', jwt);
        expect(res.status).to.equal(200);
        expect(res.body).has.property("result");
        expect(res.body.result).to.equal(true);
    })

    // it('Put /nft/property: expect 400 >> duplicated trait_type', async ()=>{
    //     let res = await chai.request(server)
    //     .put(`/nft/property`)
    //     .send({'trait_type': 'test', 'value': 't'})
    //     .set('Authorization', jwt);
    //     expect(res.status).to.equal(400);
    //     expect(res.body).has.property("message");
    //     expect(res.body.message).to.equal("duplicated trait_type");
    // })

    // it('Put /nft/property: expect 400 >> unavailable trait_type', async ()=>{
    //     let res = await chai.request(server)
    //     .put(`/nft/property`)
    //     .send({'trait_type': 'Ttest 123t', 'value': 'test'})
    //     .set('Authorization', jwt);
    //     expect(res.status).to.equal(400);
    //     expect(res.body).has.property("message");
    //     expect(res.body.message).to.equal("unavailable trait_type");
    // })

    // it('Put /nft/property: expect 400 >> unavailable trait_type(cannot use exp, grade, meta)', async ()=>{
    //     let res = await chai.request(server)
    //     .put(`/nft/property`)
    //     .send({'trait_type': 'exp', 'value': 100})
    //     .set('Authorization', jwt);
    //     expect(res.status).to.equal(400);
    //     expect(res.body).has.property("message");
    //     expect(res.body.message).to.equal("unavailable trait_type");
    // })
   
    // it('test regexp', ()=>{
    //     let ex = /^[A-Za-z]+((\s|_)*[A-Za-z])*$/i;
    //     let w1= 'test';
    //     let w2= 'test ';
    //     let w3= 'test-';
    //     let w4= 'test_';
    //     let w5= 'test-sdfsd';
    //     let w6= 'Ttest 123t';
    //     let w7= 'Ttest_123t';
    //     let w8= 'Ttest test';
    //     let w9= 'Ttest_test';
    //     let w10= 'Ttest test test';
    //     let w11= 'Ttest_test_test';
    //     console.log(w1, ex.test(w1));
    //     console.log(w2, ex.test(w2));
    //     console.log(w3, ex.test(w3));
    //     console.log(w4, ex.test(w4));
    //     console.log(w5, ex.test(w5));
    //     console.log(w6, ex.test(w6));
    //     console.log(w7, ex.test(w7));
    //     console.log(w8, ex.test(w8));
    //     console.log(w9, ex.test(w9));
    //     console.log(w10, ex.test(w10));
    //     console.log(w11, ex.test(w11));
    // });

    // it('Delete /nft/property: expect 200', async ()=>{
    //     let res = await chai.request(server)
    //     .delete(`/nft/property`)
    //     .send({'trait_type': 'Test'});
    //     expect(res.status).to.equal(200);
    //     expect(res.body).has.property("result");
    //     expect(res.body.result).to.equal(true);
    // })

    // it('Delete /nft/property: expect 400 >> not exist trait_type', async ()=>{
    //     let res = await chai.request(server)
    //     .delete(`/nft/property`)
    //     .send({'trait_type': 'fail_test'});
    //     expect(res.status).to.equal(400);
    //     expect(res.body).has.property("message");
    //     expect(res.body.message).to.equal("not exist trait_type");
    // })

    // it('Delete uploaded image: expect 400', async ()=>{

    //     let res = await chai.request(server)
    //             .delete(`/nft/upload/image`)
    //             .set('Authorization', jwt)
    //             .send({'token_id': 1});
    //             expect(res.status).to.equal(400);
    // });

    // it('Delete uploaded image: expect 200', async ()=>{

    //     let res = await chai.request(server)
    //             .delete(`/nft/upload/image`)
    //             .set('Authorization', jwt)
    //             .send({'token_id': 1});
    //             expect(res.status).to.equal(200);
    //             expect(res.body.result).to.equal(true);
    // });

    it('Grade Image', async()=>{
        let res = await chai.request(server)
        .get(`/nft/upload/grade-image`)
        .send({token_id:10})
        .set('Authorization', jwt);

        expect(res.status).to.equal(200);
        console.log(res.body);
    })

    it('test', ()=>{
        const exp= 15;
        const gold = 60;
        const silver = 40;
        const bronzw = 10;
        if(exp > gold){
            console.log('gold')
        }else if(exp > silver){
            console.log('silver')
        }else{
            console.log('bronze')
        }
    });

    it('upload exp => grade', async()=>{
        let res = await chai.request(server)
        .post(`/nft/upload/exp`)
        .send({token_id:1, exp: 42})
        .set('Authorization', jwt);

        expect(res.status).to.equal(200);
        console.log(res.body);
    })


})
