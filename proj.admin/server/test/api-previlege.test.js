const chai = require('chai');
const infura = require("../../../lib.common/infura");
const config = require('../config');
const { expect, assert } = chai;
const chaiHttp = require('chai-http');
const server = require('../index');
chai.use(chaiHttp);

describe('[Previlege List]', () => {

    const wallet_pk = "d74299f3cf148b0fd5a58abb9bc6c26f3d3e0e1bb17dac4600be2b84e6be5789";
    let wallet='0xF090f3401389b9b3d77871DCbe6e6330fD7D4278'; 
    let web3;
    let jwt;
    // let jwt=`eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ3YWxsZXQiOiIweEYwOTBmMzQwMTM4OWI5YjNkNzc4NzFEQ2JlNmU2MzMwZkQ3RDQyNzgiLCJuYW1lIjoiT1dORVIiLCJyaWdodHMiOlsibmZ0X3JlYWQiLCJuZnRfbWFuYWdlIiwicHJldmlsZWdlc19yZWFkIiwicHJldmlsZWdlc19tYW5hZ2UiXSwiaWF0IjoxNjg3MzM0NDY5LCJleHAiOjE2ODc5MzkyNjksImlzcyI6InNoZWVwZmFybS1hdXRoLXNlcnZlciJ9.11TFLm1K78C6s3hFZo8R2MBh1BqC3Ctd8DbPP6Szg2c`;

    before('Prepare JWT', async () => {
        web3 = infura.getWeb3('mumbai');        
        const message = config.server.SIGN_MESSAGE;
        const hash = web3.utils.soliditySha3(message, wallet);
        const signObj = await web3.eth.accounts.sign(hash, wallet_pk);
        const res = await chai.request(server)
            .post("/auth")
            .send({
                "address": wallet,
                "wallet_type": "metamask",
                "signature": signObj['signature']
            })
        jwt = res.body.jwt_token;
        console.log("generated jwt:" + jwt);
    });
    
    it('put /previlege/setup: expect 200', async () => {
        let res = await chai.request(server)
          .put(`/previlege/setup`)
          .set('Authorization', jwt) 
          .send({
            'wallet': '0xF091632a60Fbba27e2d1b12c9AC259d1f0DEBc72',
            'rights': ["previleges_manage", "nft_manage"],
            'name': 'test'
          });
        expect(res.status).to.equal(200);
        expect(res.body).to.have.property("result");
        expect(res.body.result).to.equal(true);
    });
      
    it('put /previlege/setup: expect 400 >> unavailable right', async ()=>{
        let res = await chai.request(server)
          .put(`/previlege/setup`)
          .set('Authorization', jwt)
          .send({
            'wallet': '0xF091632a60Fbba27e2d1b12c9AC259d1f0DEBc73',
            'rights': ["nft_read", "nft_test"],
            'name': 'test'
          });
        expect(res.status).to.equal(400);
        expect(res.body).has.property("message");
        expect(res.body.message).to.equal("unavailable right");
    })

    it('delete /previlege/setup: expect 200', async ()=>{
        let res = await chai.request(server)
          .delete(`/previlege/setup`)
          .set('Authorization', jwt)
          .send({
            'wallet': '0xF091632a60Fbba27e2d1b12c9AC259d1f0DEBc70'
          });
        expect(res.status).to.equal(200);
        expect(res.body).to.have.property("result");
        expect(res.body.result).to.equal(true);
    })
  
    it('delete /previlege/setup: expect 400 >> not exist wallet', async ()=>{
        let res = await chai.request(server)
          .delete(`/previlege/setup`)
          .set('Authorization', jwt)
          .send({
            'wallet': '0xF091632a60Fbba27e2d1b12c9AC259d1f0DEBc00',
          });
        expect(res.status).to.equal(400);
        expect(res.body).has.property("message");
        expect(res.body.message).to.equal("not exist wallet");
    })

})
