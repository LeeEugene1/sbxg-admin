const chai = require('chai');
const { expect, assert } = chai;
const chaiHttp = require('chai-http');
const {S3, getS3Key, getBucketUri} = require(path.join(__dirname, '../../../', './lib.common/s3'));
const cloudflare = require(path.join(__dirname, '../../../', './lib.common/cloudflare'));
const server = require('../index');
chai.use(chaiHttp);

describe('[S3, CLOUDFLARE TEST]', () => {

    const bucket = 'cdn-test.sbxg.win';
    const s3 = new S3(bucket);

    it('upload meta', async()=>{
        let img = "C:/Users/wjdgu/Desktop/test_img.png";
        let key = 'test/test_img';
        let result = await s3.uploadImage(img, key);
        console.log(result)
    });

    it('upload meta', async()=>{
        try{
            let data = {"content":"I'm data222"};
            let key = 'test/meta';
            let result = await s3.uploadMetadata(JSON.stringify(data), key);
            console.log(result)
        }catch(error){
            console.log(error)
        }
       
    });

    it('delete object', async()=>{
        try{
            let key = 'test/meta';
            let result = await s3.deleteObject(key);
            console.log(result)
        }catch(error){
            console.log(error)
        }
    });

    it.only('getS3Key, getBucketUri', async()=>{
        try{
            let url = 'https://cdn-test.sbxg.win/img/nft/hidden.png';
            console.log(getS3Key(url));
            console.log(getBucketUri(url));

        }catch(error){
            console.log(error)
        }
    });

    it('cloudflare', async()=>{
        try{
            
            const result = await cloudflare.purgeCache(['https://cdn-test.sbxg.win/img/unavailable.png']);
            console.log(result);

        }catch(error){
            console.log(error)
        }
    });
    


});