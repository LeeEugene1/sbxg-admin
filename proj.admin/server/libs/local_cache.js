const fs = require('fs');

class LocalCache {
    
    constructor(path) { 
        this.path = path;
        this.cache = null;
    }

    get_value (key, default_value = null) {        
        if (this.cache == null) {
            try {
                this.cache = JSON.parse(fs.readFileSync(this.path, 'utf8'));
                if (!this.cache)
                    throw new Error();
            } catch (e) {            
                this.cache = {};
                this.set_value(key, default_value);
            }
        }
    
        if (key in this.cache == false) {
            this.cache[key] = default_value;
            this.set_value(key, default_value);
        }
        
        return this.cache[key];
    }
    
    set_value (key, value) { 
        this.cache[key] = value;
        fs.writeFileSync(this.path, JSON.stringify(this.cache));
    }
}

module.exports = { LocalCache }