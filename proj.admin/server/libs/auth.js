const config = require("../config");
const jwt = require("jsonwebtoken");

const RIGHTS_PREVILEGES = "previleges_manage";
const RIGHTS_NFT = "nft_manage";
const RIGHTS_SETTING = "setting_manage";

const ALL_RIGHTS = [RIGHTS_PREVILEGES, RIGHTS_NFT, RIGHTS_SETTING];

let checkJwt = function () {
  return (req, res, next) => {
    try {
      let req_jwt = req.headers.authorization;
      req.user = jwt.verify(req_jwt, config.server.JWT_SECRET);
      return next();
    } catch (error) {
      // 유효기간이 초과된 경우
      if (error.name === "TokenExpiredError") {
        return res.status(419).json({
          code: 419,
          message: "expired token",
        });
      }
      console.log("invalid token");
      // 토큰의 비밀키가 일치하지 않는 경우
      return res.status(401).json({
        code: 401,
        message: "invalid token. " + error.name,
      });
    }
  };
};

let haveRights = function (rights) {
  return (req, res, next) => {
    try {
      for (let i = 0; i < rights.length; i++) {
        let right = rights[i];
        let num = req.user.rights.filter((r) => right == r);
        if (num == 0)
          return res.status(401).json({
            code: 401,
            message: `${right} right required`,
          });
      }
    } catch (error) {
      console.log("invalid rights");
      return res.status(401).json({
        code: 401,
        message: `rights error`,
      });
    }
    return next();
  };
};

let isRights = function (rights) {
  return (req, res, next) => {
    try {
      for (let i = 0; i < rights.length; i++) {
        let right = rights[i];
        let num = req.user.rights.filter((r) => right == r);
        if (num == 0) return false;
      }
    } catch (error) {
      console.log("invalid rights");
      return false;
    }
    return true;
  };
};

module.exports = {
  haveRights,
  checkJwt,
  isRights,
  ALL_RIGHTS,
  RIGHTS_PREVILEGES,
  RIGHTS_NFT,
  RIGHTS_SETTING,
};
