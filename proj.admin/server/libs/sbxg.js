const path = require("path");
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const db = require('./db');
const config = require('../config');
const {S3, getS3Key} = require(path.join(__dirname, '../../../', './lib.common/s3'));
const GOLD = 30;
const SILVER = 20;
const BRONZE = 10;
const GRADE = {
    [GOLD]: 'Gold',
    [SILVER]: 'Silver',
    [BRONZE]: 'Bronze'
}

const isOwner = (wallet)=>{ 
    if('0xF090f3401389b9b3d77871DCbe6e6330fD7D4278'.toLowerCase() == wallet.toLowerCase() ) return true;
    if('0xd2e67a446eaB1fa27467278a01911d17A3136891'.toLowerCase() == wallet.toLowerCase() ) return true;
    return false;
}

const _uploadNFTImage = multer({
    storage: multerS3({
            s3: new AWS.S3({
                accessKeyId: config.S3.ACCESS_KEY,
                secretAccessKey: config.S3.SECRET_KEY,
                region: config.S3.REGION
            }), 
            bucket: config.S3.BUCKET_NAME,
            contentType: multerS3.AUTO_CONTENT_TYPE,
            key: function (req, file, cb) {
            const token_id = req.body.token_id;
            // const name = file.originalname.split('.')[0];
            const extention = file.mimetype.split('/')[1];
            const fileName = `img/nft/artwork/${token_id}.${extention}`;
            cb(null, fileName);
            },
            acl: 'public-read',
        }),
    fileFilter: function (req, file, cb) {
        if(!file){
            return cb(new Error("empty file"));
        }
        const extention = file.mimetype.split('/')[1];
        if (!["png", "jpg", "jpeg", "gif"].includes(extention)) {
            return cb(new Error("Invalid mime type"));
        }else{
            cb(null, true);
        }
    },
}).single('file');

const uploadNFTImage = (req, res, _) => {
    return new Promise((resolve, reject) => {
        _uploadNFTImage(req, res, function (err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

/** TODO 확인 */
const deleteNFTImage = async (image_url) => { 
    if(!image_url || image_url == 'undefined'){
        throw Error('empty image url')
    }
    const s3 = new S3(config.S3.BUCKET_NAME);
    const result =  await s3.deleteObject(getS3Key(image_url));
    console.log(result)
}

const uploadMetadata = async (meta, key)=>{
    if(typeof meta != 'string') meta = JSON.stringify(meta);
    const s3 = new S3(config.S3.BUCKET_NAME);
    const result = await s3.uploadMetadata(meta, key);
    return result;
}


const _uploadCsv = multer({ 
    dest: 'tmp/',
    fileFilter: function (req, file, cb) {
        const extention = file.mimetype.split('/')[1];
        if (!["csv"].includes(extention)) {
            return cb(new Error("Invalid mime type"));
        }else{
            cb(null, true);
        }
    }
}).single('file');

const uploadCsv = (req, res, _) => {
    return new Promise((resolve, reject) => {
        _uploadCsv(req, res, function (err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

const isImageRegistered = (image_url)=>{
    if(!image_url || image_url == 'undefined'){
        throw Error('empty image url')
    }
    const regex_ipfs = /^ipfs:\/\//;
    if(regex_ipfs.test(image_url)){
        return false;
    }
    const regex = /^img\/nft\/grade\//;
    // const regex = /^img\/nft\/grade\/(10|20|30|hidden)\.png$/;
    return regex.test(getS3Key(image_url))? false : true;
}

// const getDefaultImageUrl = (grade)=>{
//     if(typeof grade != 'number') grade = parseInt(grade);
//     if(Object.keys(GRADE).includes(grade)){
//         throw Error('not exist grade');
//     }

//     return `https://${config.S3.BUCKET_NAME}/img/nft/grade/${grade}.png`;
// }

const getDefaultImageUrl = (grade, token_id)=>{
    if(typeof grade != 'number') grade = parseInt(grade);
    if(typeof token_id != 'number') token_id = parseInt(token_id);
    if(Object.keys(GRADE).includes(grade)){
        throw Error('not exist grade');
    }
    return `https://${config.S3.BUCKET_NAME}/img/nft/grade/${grade}/${token_id}.png`;
}

const getGradeFromExp = (exp_range, exp) => {

    if(typeof exp != 'number') exp = parseInt(exp);

    if(exp <= exp_range[BRONZE]['to']){
        return BRONZE;
    }else if(exp <= exp_range[SILVER]['to']){
        return SILVER;
    }else { // gold 는 우선 제한 없도록 함
        return GOLD;
    }
}

module.exports ={
    GOLD, SILVER, BRONZE,
    isOwner,
    getGradeName: (num)=>{
        if(num == BRONZE) return 'Bronze';
        if(num == SILVER) return 'Silver';
        if(num == GOLD) return 'Gold';
        return 'Bronze';
    },
    uploadNFTImage,
    deleteNFTImage,
    uploadMetadata,
    uploadCsv,
    isImageRegistered,
    getDefaultImageUrl,
    getGradeFromExp,
}