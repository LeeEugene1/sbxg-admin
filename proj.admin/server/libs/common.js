const { db } = require("./db.js");
const crypto = require('crypto');

get_admin = async (address) => {
  let admins_json = await db.one(
    "SELECT `value` FROM TB_CONFIG WHERE `key`='admin'"
  );
  let ADMINS = admins_json.value;
  for (let admin_addr in ADMINS) {
    if (admin_addr.toLowerCase() == address.toLowerCase())
      return ADMINS[admin_addr];
  }
  return null;
};

insertAdmLog = async (admin_id, api, api_param) => {
  const query = `INSERT INTO TB_ADM_LOG SET created_at=NOW(), admin_id =?, api=?, api_param=?`;
  await db.query(query, [admin_id, api, api_param]);
};

createAdmId = (address) => {
  return address.replace("0x", "").slice(0, 4);
};


encrypt=(text, key)  => {
  const _key = crypto.createHash('sha256').update(key).digest();
  const iv = crypto.randomBytes(16); // Generate a random IV (Initialization Vector)
  const cipher = crypto.createCipheriv('aes-256-cbc', _key, iv);
  let encrypted = cipher.update(text, 'utf8', 'hex');
  encrypted += cipher.final('hex');
  return iv.toString('hex') + encrypted;
}

decrypt=(encryptedText, key) =>  {
  const _key = crypto.createHash('sha256').update(key).digest();
  const iv = Buffer.from(encryptedText.slice(0, 32), 'hex'); // Extract IV from the encrypted text
  const decipher = crypto.createDecipheriv('aes-256-cbc', _key, iv);
  let decrypted = decipher.update(encryptedText.slice(32), 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
}

module.exports = { get_admin, insertAdmLog, createAdmId, encrypt, decrypt };
