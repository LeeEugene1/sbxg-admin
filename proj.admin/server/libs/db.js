const config = require("../config");
const mysql = require("../../../lib.common/mysql");

const db = new mysql.MySQL(
  config.db.HOST,
  config.db.USER,
  config.db.PASS,
  config.db.DB_NAME
);

module.exports = { db, ...mysql };
