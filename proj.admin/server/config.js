const path = require("path");
const checkEnv = require("check-env");
const common_config = require(path.join(__dirname, '../../', './lib.common/config'));
require("dotenv").config({ path: path.join(__dirname, ".env") });

checkEnv(['POLYGON_NETWORK', 'SYNCER_CONTEXT', 'JWT_SECRET', 'PORT']);
checkEnv(['MYSQL_HOST', 'MYSQL_USER', 'MYSQL_PASS', 'MYSQL_DB_NAME']);

console.log("DB_NAME:", process.env.MYSQL_DB_NAME);
console.log("POLYGON_NETWORK:", process.env.POLYGON_NETWORK);
console.log("SYNCER_CONTEXT:", process.env.SYNCER_CONTEXT);


module.exports = {
  server: {
    PORT: process.env.PORT,
    POLYGON_NETWORK: process.env.POLYGON_NETWORK,
    JWT_SECRET: process.env.JWT_SECRET,
    SIGN_MESSAGE: common_config.server.SIGN_MESSAGE,
  },

  db: {
    HOST: process.env.MYSQL_HOST,
    USER: process.env.MYSQL_USER,
    PASS: process.env.MYSQL_PASS,
    DB_NAME: process.env.MYSQL_DB_NAME,
  },

  db_local: {
    HOST: "localhost",
    USER: "root",
    PASS: "jhj60081406",
    DB_NAME: "sbxg",
  },

  sync_owner: {
    SYNCER_CONTEXT: process.env.SYNCER_CONTEXT
  },

  networks: common_config.networks,
  S3: common_config.S3,

};
