const path = require('path');
const cron = require('node-cron');
const config = require("../config");
const infura = require(path.join(__dirname, '../../../', './lib.common/infura'));
const { MySQL } = require('../libs/db');
const { LocalCache } = require("../libs/local_cache");

const set = async (context)=>{
    if(context == infura.NETWORKS.MAINNET){
        const network = infura.NETWORKS.MAINNET;
        const web3 = infura.getWeb3(network);
        return {
            network: network,
            web3: web3,
            db: new MySQL(config.db.HOST, config.db.USER, config.db.PASS, config.db.DB_NAME),
            nft_contract:  infura.sbxg.getMgzContract(web3, network),
            cache: new LocalCache(path.basename(__filename).replace(".js", "_live")),
            interval: 10000,
            start_block: 44507780 
        }

    }else if(context == infura.NETWORKS.MUMBAI){
        const network = infura.NETWORKS.MUMBAI;
        const web3 = infura.getWeb3(network);
        const DB_NAME = config.db.DB_NAME === 'sbxg'? 'sbxg_test' : config.db.DB_NAME;
        return {
            network: network,
            web3: web3,
            db: new MySQL(config.db.HOST, config.db.USER, config.db.PASS, DB_NAME),
            nft_contract:  infura.sbxg.getMgzContract(web3, network),
            cache: new LocalCache(path.basename(__filename).replace(".js", "_test")),
            interval: 10000,
            start_block: 36741742 
        }

    }else{  /**local */
        const network = infura.NETWORKS.MUMBAI;
        const web3 = infura.getWeb3(network);
        return {
            network: network,
            web3: web3,
            db: new MySQL(config.db_local.HOST, config.db_local.USER, config.db_local.PASS, config.db_local.DB_NAME),
            nft_contract:  infura.sbxg.getMgzContract(web3, network),
            cache: new LocalCache(path.basename(__filename).replace(".js", "_local")),
            interval: 10000,
            start_block: 36752455 
        }
    }
}

const saveEvents = async(cf, events)=>{

    const transaction = await cf.db.beginTransaction();
    try{
        
        for (let i = 0; i < events.length; i++) {
            const event = events[i];
            let transfer_to = event.returnValues.to;
            let transfer_from = event.returnValues.from;
            let token_id = event.returnValues.tokenId;
            let tx_hash = event.transactionHash;
            let block_number = event.blockNumber;
            let log_index = event.logIndex;
    
            await transaction.query(`UPDATE TB_NFT 
                    SET holder = ?, 
                        last_holder = ?,
                        block_number = ?,
                        updated_at = NOW()
                    WHERE 
                        token_id=? 
                        AND ( block_number<? OR block_number IS NULL )`,
                    [transfer_to, transfer_from, block_number, token_id, block_number]);
            
            await transaction.query(`INSERT INTO TB_NFT_TRANSFER SET tx_hash=?, token_id=?, block_number=?, transfer_from=?, transfer_to=?, log_index=? 
                                ON DUPLICATE KEY UPDATE block_number=?, transfer_from=?, transfer_to=?, log_index=?`,
                            [tx_hash, token_id, block_number, transfer_from, transfer_to, log_index, block_number, transfer_from, transfer_to, log_index]);
        }

        await transaction.commit();

    }catch(error){
        await transaction.rollback();
        throw Error(error);
    }
}

const handleEvents = async (cf)=>{ 

    const BATCH_SIZE = 30;

    try{
        const last_block = cf.cache.get_value("last_block", cf.start_block);
        const curr_block = await infura.getBlockNumber(cf.web3);
        if (last_block >= curr_block) return;
    
        const from_block = Math.min(last_block+1, curr_block);
        const to_block = Math.min(last_block + BATCH_SIZE, curr_block);  
        console.log(`Transfer at ${from_block}~${to_block}. remains:${curr_block - from_block} batch_size:${BATCH_SIZE}`);
    
        let events = await cf.nft_contract.getPastEvents('Transfer', { "fromBlock":from_block, "toBlock":to_block });
        console.log(`event found ${events.length}`);
    
        if(events.length > 0)
            await saveEvents(cf, events);
       
        cf.cache.set_value("last_block", to_block);

    }catch(error){
        console.log(`${error}`);
    }
   
}

const fix_all = async (cf) => { 
    // 기존에 수집된 TB_NFT_TRANSFER 를 이용해서 TB_NFT.owner 와 TB_NFT.blockNumber 를 교정.
    // 주의. 매우 무거운 쿼리
    console.log("start fix all");
    let resetQuery = `
                    UPDATE
                        TB_NFT AS dest,
                        (
                            WITH last_block AS (
                                SELECT t.token_id, t.block_number, t.transfer_to AS holder, t.transfer_from AS sender, t.log_index
                                FROM TB_NFT_TRANSFER AS t
                                INNER JOIN (
                                    SELECT token_id, MAX(block_number) AS block_number
                                    FROM TB_NFT_TRANSFER
                                    GROUP BY token_id
                                ) AS a ON a.token_id = t.token_id AND t.block_number = a.block_number
                            )		    
                            SELECT token_id, block_number, holder, sender
                            FROM last_block
                            WHERE (token_id, block_number, log_index) IN (
                              SELECT token_id, block_number, MAX(log_index)
                              FROM last_block
                              GROUP BY token_id, block_number
                            )
                        ) AS src
                    SET dest.block_number = src.block_number,
                        dest.holder       = src.holder,
                        dest.last_holder  = src.sender
                    WHERE dest.token_id = src.token_id
                    `;

    let updateResult = await cf.db.query(resetQuery);
    console.log("result" + JSON.stringify(updateResult));
}

const run = async ()=>{
    console.log(`Start sync nft owner>> context:${config.sync_owner.SYNCER_CONTEXT}, network:${config.server.POLYGON_NETWORK}`)
    const cf = await set(config.sync_owner.SYNCER_CONTEXT || "mumbai");
    await fix_all(cf);
    const task = cron.schedule(`*/30 * * * * *`, async()=>{
        await handleEvents(cf);
    })
    task.start();
}

run();