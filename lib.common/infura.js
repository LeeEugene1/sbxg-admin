const Web3 = require("web3");
const config=require("./config");

const ABI = {
    MGZ: require("./abi/MgzNft.json"),
    MGZ_MINT_PROXY: require("./abi/MgzNftProxy.json"),
}

const NETWORKS = {
    MUMBAI: config.networks.MUMBAI,   
    MAINNET: config.networks.MAINNET
}

const CA = {
    MAINNET: {
        MGZ: "0x4494E3488d3BA60CEA49462DC632790f0bA28F67",
        MGZ_MINT_PROXY: "0xd8643207025ABCE093A77C84f1f1534dE2E95C20",
    },

    MUMBAI: {
        MGZ: "0x532E109baa7D8f6d3dDfa2d50495988D5f073924",
        MGZ_MINT_PROXY: "0x719876ecEFF13db8b64A05c2E4D61AB2677CeAf9"
    },
}

/**
 * 블록 넘버를 가져옵니다. 
 * @param {web3} web3 
 * @returns {string}
 */
let getBlockNumber = async (web3) => {    
    let blockNumber = await web3.eth.getBlockNumber();
    return blockNumber;
}

let getWeb3 = (network) => { 
    
    if (network == NETWORKS.MAINNET) {
        return new Web3(new Web3.providers.HttpProvider(`https://polygon-mainnet.infura.io/v3/${config.INFURA.API_KEY}`));
    } else {
        return new Web3(new Web3.providers.HttpProvider(`https://polygon-mumbai.infura.io/v3/${config.INFURA.API_KEY}`));
    }
}

class sbxg {

    static getMgzCa(network) { return (network == NETWORKS.MAINNET ? CA.MAINNET.MGZ : CA.MUMBAI.MGZ); }
    static getMgzContract(web3, network) {
        const ca = sbxg.getMgzCa(network);
        const abi = ABI.MGZ;
        let contract = new web3.eth.Contract(abi, ca);
        return contract;
    }

    static getMgzMintProxyCa(network) { return (network == NETWORKS.MAINNET ? CA.MAINNET.MGZ_MINT_PROXY : CA.MUMBAI.MGZ_MINT_PROXY); }
    static getMgzMintProxyContract(web3, network) {
        const ca = sbxg.getMgzMintProxyCa(network);
        const abi = ABI.MGZ_MINT_PROXY;
        let contract = new web3.eth.Contract(abi, ca);
        return contract;
    }

}


module.exports = {
    NETWORKS,
    ABI,
    CA,
    getBlockNumber,
    getWeb3,
    sbxg
}