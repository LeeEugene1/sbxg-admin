
const config = require('./config');
const axios = require('axios');

const zone_id = config.CLOUD_FLARE.ZONE_ID;
const auth_email = config.CLOUD_FLARE.AUTH_EMAIL;
const auth_key = config.CLOUD_FLARE.AUTH_KEY; // API key(global)

module.exports = {

    /**
     * @param {Array} purge_urls e.g. ['https://...', 'https://...']
     * @returns 
     */
    purgeCache: async (purge_urls)=>{ 
        
        if(purge_urls.length>0){

            const url = `https://api.cloudflare.com/client/v4/zones/${zone_id}/purge_cache`;
            const headers = {'Content-Type': 'application/json',
                            'X-Auth-Email': auth_email,
                            'X-Auth-Key': auth_key };
            const data = JSON.stringify({"files": purge_urls});

            const result = await axios.post(url, data, {
                headers: headers
            });

            return result.data;

        }else{

            return {
                success: false,
                messages: 'empty purge_urls'
              };
        }

    }

}