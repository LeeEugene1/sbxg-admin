const fs = require("fs");
const AWS = require('aws-sdk');
const config = require("./config");

class S3 { 

    constructor(bucket) {
        const accessKeyId = config.S3.ACCESS_KEY
        const secretAccessKey = config.S3.SECRET_KEY
        const region = config.S3.REGION  
        this.bucket = bucket;
        this.s3 = new AWS.S3({ accessKeyId, secretAccessKey, region });
    }

    uploadImage = async (filePath, key) => {
        var param = {
            'Bucket': this.bucket,
            'Key': key,
            'ACL': 'public-read',
            'Body': fs.createReadStream(filePath),
        }
        return new Promise((resolve, reject) => {
            this.s3.upload(param, function (err, data) {
                if (err) {
                    return reject(err);
                }
                return resolve(data)
            });
        })
    }

    uploadMetadata = async (text, key) => {
        return new Promise((resolve, reject) => {
            this.s3.upload({
                'Bucket': this.bucket,
                'Key': key,
                'Body': text,
                'ACL': 'public-read',
                'ContentType': "application/json",
            }, function (err, data) {
                if (err) {
                    return reject(err);
                }
                return resolve(data)
            });
        });
    }

    deleteObject = async (key) => { 
        return new Promise((resolve, reject) => { 
            this.s3.deleteObject({
                Bucket: this.bucket, 
                Key: key
            }, function (err, data) { 
                if (err) {
                    return reject(err);
                }
                return resolve(data)
            })
        })
    }
    
}

function getS3Key(uri) { // bucket 이후 경로
    let tokens = uri.split("/");
    let idx = tokens.indexOf(tokens[3]); 
    let key = tokens.slice(idx).join("/");
    return key;
}

function getBucketUri(uri) {
    let tokens = uri.split("/");
    let bucket = tokens[2];
    return bucket;
}


module.exports = {S3, getS3Key, getBucketUri }
