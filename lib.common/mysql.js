const _ = require('mysql2')
const mysql = require('mysql2/promise')

class MySQL {
    constructor(host, id, pass, database) {
        this.pool = mysql.createPool({
            host: host,
            user: id,
            password: pass,
            database: database,
            waitForConnections: true,
            connectionLimit: 10,
            queueLimit: 0
        });
    }

    async query(sql, args, conn = null, print_log = false) {
        let retry_cnt = 0;
        while (1) {
            let _conn = conn || await this.pool.getConnection()
            try {
                print_log ? console.log(_.format(sql, args), _conn.connection.connectionId) : null
                const [rows, fields] = await _conn.query(sql, args);
                conn ? null : _conn.release();
                return rows
            } catch (err) {
                if (err.errno != 1644) {
                    console.error("====================================================")
                    console.error(_.format(sql, args), _conn.connection.connectionId)
                    console.error("err.message", err.errno, "retry_cnt", retry_cnt)
                    console.error("====================================================")
                }

                conn ? null : _conn.release();
                if (retry_cnt > 5) {
                    throw err
                }
                if (err.errno == 1213) {
                    await new Promise(r => setTimeout(r, 50))
                    retry_cnt++;
                } else if (err.errno == "ECONNRESET") {
                    await new Promise(r => setTimeout(r, 1000))
                } else {
                    console.log("err.errno > ", err.errno)
                    throw err
                }
            }
        }
    }

    async beginTransaction() {
        let conn = await this.pool.getConnection()
        await conn.beginTransaction();

        return {
            isTransaction: true,
            commit: async () => {
                await conn.commit();
                conn.release();
            },
            rollback: async () => {
                await conn.rollback();
                conn.release();
            },
            release: () => {
                conn.release();
            },
            query: (sql, args, print) => {
                return this.query(sql, args, conn, print)
            },
            one: (sql, args) => {
                return this.one(sql, args, conn)
            },
            format: (...a) => this.format(...a)
        }
    }

    async commit() {
        console.log("[commit] not transaction connect")
    }

    async rollback() {
        console.log("[rollback] not transaction connect")
    }

    async release() {
        console.log("[release] not transaction connect")
    }

    async one(...args) {
        let rows = await this.query(...args)
        return rows[0]
    }

    format(sql, args) {
        return _.format(sql, args)
    }

}

function assert_changed_row(result, count) {
    if (result.changedRows != count)
        throw new Error("query failed");
}

function assert_upserted(result) {
    /**
     * With ON DUPLICATE KEY UPDATE, the affected-rows value per row is 1 if the row is inserted as a new row and 2 if an existing row is updated.
     */
    if (result.affectedRows != 1 && result.affectedRows != 2)
        throw new Error("upsert failed");
}

function assert_inserted(result) {
    /**
     * With ON DUPLICATE KEY UPDATE, the affected-rows value per row is 1 if the row is inserted as a new row and 2 if an existing row is updated.
     */
    if (result.affectedRows != 1)
        throw new Error("insert failed");
}

function assert_delete_count(result, count) {
    if (result.affectedRows != count)
        throw new Error("delete failed");
}

module.exports = {
    MySQL,
    assert_changed_row,
    assert_upserted,
    assert_inserted,
    assert_delete_count
}
