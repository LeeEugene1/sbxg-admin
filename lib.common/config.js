const path = require("path");
const checkEnv = require("check-env");
require("dotenv").config({ path: path.join(__dirname, ".env") });

checkEnv(["INFURA_API_KEY"]);
checkEnv([
  "AWS_S3_ACCESS_KEY",
  "AWS_S3_SECRET_KEY",
  "AWS_S3_REGION",
  "AWS_S3_BUCKET_NAME",
]);
checkEnv([
  "CLOUD_FLARE_ZONE_ID",
  "CLOUD_FLARE_AUTH_EMAIL",
  "CLOUD_FLARE_AUTH_KEY",
]);

console.log("AWS_S3_BUCKET_NAME:", process.env.AWS_S3_BUCKET_NAME);

module.exports = {
  server: {
    POLYGON_NETWORK: process.env.POLYGON_NETWORK,
    SIGN_MESSAGE: process.env.AUTH_SIGN,
  },

  S3: {
    SECRET_KEY: process.env.AWS_S3_SECRET_KEY,
    ACCESS_KEY: process.env.AWS_S3_ACCESS_KEY,
    REGION: process.env.AWS_S3_REGION,
    BUCKET_NAME: process.env.AWS_S3_BUCKET_NAME,
  },

  INFURA: { API_KEY: process.env.INFURA_API_KEY },

  CLOUD_FLARE: {
    ZONE_ID: process.env.CLOUD_FLARE_ZONE_ID,
    AUTH_EMAIL: process.env.CLOUD_FLARE_AUTH_EMAIL,
    AUTH_KEY: process.env.CLOUD_FLARE_AUTH_KEY,
  },

  networks: {
    MUMBAI: "mumbai",
    MAINNET: "mainnet",
  },
};
