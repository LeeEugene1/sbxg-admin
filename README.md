# SBXG
## 서버배포
git pull

## proj.admin - server
.env
```
PORT=500
MYSQL_HOST=
MYSQL_USER=
MYSQL_PASS=
MYSQL_DB_NAME=sbxg_test
JWT_SECRET=123123JWT_SECRETJWT_SECRETJWT_SECRETJWT_SECRET

AWS_S3_SECRET_KEY=
AWS_S3_ACCESS_KEY=
AWS_S3_REGION=ap-northeast-2
AWS_S3_BUCKET_NAME=cdn-test.sbxg.win

CLOUD_FLARE_ZONE_ID=
CLOUD_FLARE_AUTH_EMAIL=mtg@sbxg.gg
CLOUD_FLARE_AUTH_KEY=

INFURA_API_KEY=

SYNCER_CONTEXT=local
POLYGON_NETWORK=mumbai

```

## proj.admin - client
prettier적용
파일을 저장할때마다 .prettierrc 조건으로 코드포멧을 합니다.
만약 저장후에도 아무 변화가 없다면 vscode setting.json을 열어 아래 코드 추가합니다.
```
    "[javascript]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[javascriptreact]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[typescript]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[typescriptreact]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "editor.formatOnSave": true,
```
.env-cmdrc
```
{
    "development": {
        "REACT_APP_URL": "http://localhost:500/",
        "REACT_APP_AUTH": 
    },
    "qa": {
        "REACT_APP_URL": 
        "REACT_APP_AUTH": 
    },
    "production": {
        "REACT_APP_URL": 
        "REACT_APP_AUTH": 
    }
}
```